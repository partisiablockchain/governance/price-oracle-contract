# Price Oracle Contract

## Description

To get information from Ethereum to PBC we need an oracle to keep an eye on
the [ETH / USD Chainlink Reference Contract](https://docs.chain.link/docs/selecting-data-feeds) and
report price changes to PBC.

## Diagrams

### Register

```mermaid
%%{init: { 'sequence': {'mirrorActors':true} } }%%
sequenceDiagram
    actor A as PO Node A
    participant PO as PO Contract
    participant LO as LO Contract
    participant AP as Account Plugin
    A->>LO: ASSOCIATE_TOKENS_TO_CONTRACT
    activate LO
    LO->>AP: ASSOCIATE_TOKENS_TO_CONTRACT
    AP-->>LO: ASSOCIATE_TOKENS_CALLBACK
    LO-->>LO: Mark tokens as associated
    deactivate LO
    A->>PO: REGISTER
    activate PO
    PO->>LO: LOCK_TOKENS_TO_PRICE_ORACLE
    activate LO
    LO-->>LO: Mark the tokens as locked to<br/> the price oracle contract
    LO-->>PO: TOKENS_LOCKED_CALLBACK
    deactivate LO
    PO-->>PO: Add PO Node A to registered nodes
    deactivate PO
```

### Notify price update

When enough agreeing price updates for the same round has been received a challenge period is
started. The challenge period takes one hour and additional price updates are allowed during the
challenge period.

```mermaid
%%{init: { 'sequence': {'mirrorActors':true} } }%%
sequenceDiagram
    actor A as PO Node A
    actor B as PO Node B
    actor C as PO Node C
    actor D as PO Node D
    participant PO as PO Contract
    participant AP as Account Plugin
    A->>PO: NOTIFY_PRICE_UPDATE(R:1, P:1)
    B->>PO: NOTIFY_PRICE_UPDATE(R:1, P:1)
    C->>PO: NOTIFY_PRICE_UPDATE(R:1, P:1)
    activate PO
    activate PO
    Note right of PO: Start challenge period
    %%PO-->>PO: Start challenge period
    D->>PO: NOTIFY_PRICE_UPDATE(R:1, P:1)
    Note right of PO: challenge period ended
    deactivate PO
    %%PO-->>PO: End challenge period
    PO-->>AP: Reward [A,B,C,D]
    PO->>AP: SET_COIN(P:1)
    deactivate PO
```

### Disputes

Notifying a disagreeing price for a round at any point before the challenge period has
ended should
begin a dispute.

```mermaid
%%{init: { 'sequence': {'mirrorActors':true} } }%%
sequenceDiagram
    actor A as PO Node A
    actor B as PO Node B
    participant PO as PO Contract
    participant LO as LO Contract
    actor LONS as LO Nodes
    participant AP as Account Plugin
    A->>PO: NOTIFY_PRICE_UPDATE(R:1, P:1)
    %% B Notifies wrong price
    B->>+PO: NOTIFY_PRICE_UPDATE(R:1, P:2)
    rect rgba(0, 0, 255, .1)
    Note over PO,LONS: Dispute logic
    PO->>+LO: CREATE_DISPUTE_POLL(R:1)
    LONS->>LO: VOTE_ON_DISPUTE(R:1, P:1)
    Note over LONS: ...
    LONS->>LO: VOTE_ON_DISPUTE(R:1, P:1)
    LO->>-PO: RESULT(P:1)
    PO->>LO: BURN_STAKED_TOKENS([B])
    PO-->>PO: Remove PO Node B
    end
    PO-->>AP: Reward PO Node A
    PO->>-AP: SET_COIN(P:1)
```

A dispute should also be started if a disagreeing price is received during a challenge period.

```mermaid
%%{init: { 'sequence': {'mirrorActors':true} } }%%
sequenceDiagram
    actor A as PO Node A
    actor B as PO Node B
    actor C as PO Node C
    actor D as PO Node D
    participant PO as PO Contract
    participant LO as LO Contract
    participant AP as Account Plugin
    A->>PO: NOTIFY_PRICE_UPDATE(R:1, P:1)
    B->>PO: NOTIFY_PRICE_UPDATE(R:1, P:1)
    C->>+PO: NOTIFY_PRICE_UPDATE(R:1, P:1)
    activate PO
    activate PO
    Note right of PO: challenge period started
    %%PO-->>PO: Start challenge period
    %% Wrong price from D
    D->>PO: NOTIFY_PRICE_UPDATE(R:1, P:2)
    rect rgba(0, 0, 255, .1)
    deactivate PO
    Note over PO,LO: Dispute logic omitted
    end
    PO-->>AP: Reward [A,B,C]
    PO->>AP: SET_COIN(P:1)
    deactivate PO
```

If the correct price for the round has not been received by any node, the large oracle members will
add a dispute counterclaim.

```mermaid
%%{init: { 'sequence': {'mirrorActors':true} } }%%
sequenceDiagram
    actor A as PO Node A
    actor B as PO Node B
    participant PO as PO Contract
    participant LO as LO Contract
    actor LONS as LO Nodes
    participant AP as Account Plugin
    A->>PO: NOTIFY_PRICE_UPDATE(R:1, P:2)
    %% B Notifies wrong price
    B->>+PO: NOTIFY_PRICE_UPDATE(R:1, P:3)
    rect rgba(0, 0, 255, .1)
    Note over PO,LONS: Dispute logic
    PO->>+LO: CREATE_DISPUTE_POLL(R:1)
    LONS->>LO: ADD_DISPUTE_COUNTER_CLAIM(R:1, P:1)
    LO->>PO: ADD_DISPUTE_COUNTER_CLAIM(R:1, P:1)
    LONS->>LO: VOTE_ON_DISPUTE(R:1, P:1)
    Note over LONS: ...
    LONS->>LO: VOTE_ON_DISPUTE(R:1, P:1)
    LO->>-PO: RESULT(P:1)
    PO->>LO: BURN_STAKED_TOKENS([B])
    PO-->>PO: Remove PO Node B
    end
    PO-->>AP: Reward PO Node A
    PO->>-AP: SET_COIN(P:1)
```