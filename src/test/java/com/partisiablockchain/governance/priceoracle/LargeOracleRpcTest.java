package com.partisiablockchain.governance.priceoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.math.Unsigned256;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.List;
import org.junit.jupiter.api.Test;

final class LargeOracleRpcTest {

  @Test
  void register() {
    BlockchainAddress sender =
        BlockchainAddress.fromString("000000000000000000000000000000000000000001");

    byte[] rpc = SafeDataOutputStream.serialize(LargeOracleRpc.lockTokensToPriceOracle(sender));

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(LargeOracleRpc.LOCK_TOKENS_TO_PRICE_ORACLE);
    assertThat(stream.readLong()).isEqualTo(LargeOracleRpc.REGISTER_STAKED_TOKENS);
    assertThat(BlockchainAddress.read(stream)).isEqualTo(sender);
  }

  @Test
  void deregister() {
    BlockchainAddress sender =
        BlockchainAddress.fromString("000000000000000000000000000000000000000001");

    byte[] rpc = SafeDataOutputStream.serialize(LargeOracleRpc.unlockTokensFromPriceOracle(sender));

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(LargeOracleRpc.UNLOCK_TOKENS_FROM_PRICE_ORACLE);
    assertThat(stream.readLong()).isEqualTo(LargeOracleRpc.REGISTER_STAKED_TOKENS);
    assertThat(BlockchainAddress.read(stream)).isEqualTo(sender);
  }

  @Test
  void burnTokens() {
    BlockchainAddress oracle =
        BlockchainAddress.fromString("000000000000000000000000000000000000000001");
    BlockchainAddress oracle2 =
        BlockchainAddress.fromString("000000000000000000000000000000000000000002");
    List<BlockchainAddress> oraclesWithIncorrectAnswer = List.of(oracle, oracle2);

    byte[] rpc =
        SafeDataOutputStream.serialize(LargeOracleRpc.burnStakedTokens(oraclesWithIncorrectAnswer));

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(LargeOracleRpc.BURN_STAKED_TOKENS);
    assertThat(BlockchainAddress.LIST_SERIALIZER.readDynamic(stream))
        .isEqualTo(oraclesWithIncorrectAnswer);
    assertThat(stream.readLong()).isEqualTo(LargeOracleRpc.REGISTER_STAKED_TOKENS);
  }

  @Test
  void setCoin() {
    String symbol = "ETH";
    PricePublisher.ConversionRate conversionRate = new PricePublisher.ConversionRate(10, 1000);

    byte[] rpc = PricePublisher.setCoin(symbol, conversionRate);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(PricePublisher.SET_COIN);
    assertThat(stream.readString()).isEqualTo(symbol);
    assertThat(stream.readLong()).isEqualTo(10);
    assertThat(stream.readLong()).isEqualTo(1000);
  }

  @Test
  void createDispute() {
    BlockchainAddress challenger =
        BlockchainAddress.fromString("000000000000000000000000000000000000000001");
    Unsigned256 roundId = Unsigned256.TEN;

    byte[] rpc =
        SafeDataOutputStream.serialize(LargeOracleRpc.createDisputePoll(challenger, roundId));

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(LargeOracleRpc.CREATE_DISPUTE_POLL);
    assertThat(BlockchainAddress.read(stream)).isEqualTo(challenger);
    assertThat(stream.readLong()).isEqualTo(0);
    assertThat(stream.readLong()).isEqualTo(0);
    assertThat(stream.readLong()).isEqualTo(10);
    assertThat(stream.readUnsignedByte()).isEqualTo(PriceOracleContract.Invocations.DISPUTE_RESULT);
    assertThat(stream.readUnsignedByte())
        .isEqualTo(PriceOracleContract.Invocations.DISPUTE_COUNTER_CLAIM);
  }

  @Test
  void createDisputeWithCost() {
    BlockchainAddress challenger =
        BlockchainAddress.fromString("000000000000000000000000000000000000000001");
    Unsigned256 roundId = Unsigned256.TEN;

    byte[] rpc =
        SafeDataOutputStream.serialize(
            LargeOracleRpc.createDisputePoll(
                challenger, roundId, LargeOracleRpc.REGISTER_STAKED_TOKENS));

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(LargeOracleRpc.CREATE_DISPUTE_POLL);
    assertThat(BlockchainAddress.read(stream)).isEqualTo(challenger);
    assertThat(stream.readLong()).isEqualTo(LargeOracleRpc.REGISTER_STAKED_TOKENS);
    assertThat(stream.readLong()).isEqualTo(0);
    assertThat(stream.readLong()).isEqualTo(10);
    assertThat(stream.readUnsignedByte()).isEqualTo(PriceOracleContract.Invocations.DISPUTE_RESULT);
    assertThat(stream.readUnsignedByte())
        .isEqualTo(PriceOracleContract.Invocations.DISPUTE_COUNTER_CLAIM);
  }
}
