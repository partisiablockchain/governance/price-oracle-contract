package com.partisiablockchain.governance.priceoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.math.Unsigned256;
import org.junit.jupiter.api.Test;

final class LargeOracleDisputeIdTest {

  @Test
  void fromRoundId() {
    Unsigned256 roundId = Unsigned256.create("92233720368547801680");
    LargeOracleDisputeId largeOracleDisputeId = LargeOracleDisputeId.fromRoundId(roundId);
    assertThat(largeOracleDisputeId.oracleId()).isEqualTo(5);
    assertThat(largeOracleDisputeId.disputeId()).isEqualTo(43600);
  }

  @Test
  void toRoundId() {
    LargeOracleDisputeId largeOracleDisputeId = new LargeOracleDisputeId(5, 43600);
    assertThat(largeOracleDisputeId.toRoundId())
        .isEqualTo(Unsigned256.create("92233720368547801680"));
  }
}
