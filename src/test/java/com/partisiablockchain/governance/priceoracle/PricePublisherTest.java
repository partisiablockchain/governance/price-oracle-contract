package com.partisiablockchain.governance.priceoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.math.Unsigned256;
import org.junit.jupiter.api.Test;

final class PricePublisherTest {

  @Test
  void conversionRateGivesOneHundredThousandGasPerOneUsd() {
    // 1 ETH = 1 USD
    Unsigned256 oneEthInUsdWithEightDecimals = Unsigned256.create(1_00000000L);
    Unsigned256 oneUsdInWei = calculateOneUsdInWei(oneEthInUsdWithEightDecimals);
    PricePublisher.ConversionRate conversionRate =
        PricePublisher.convert(oneEthInUsdWithEightDecimals, new Decimals(18, 8));
    Unsigned256 gas = externalCoinsToGas(oneUsdInWei, conversionRate);
    assertThat(gas).isEqualTo(Unsigned256.create(100_000L));
  }

  @Test
  void conversionRateGivesOneHundredThousandGasPerOneUsd_differentNumberOfDecimals() {
    // For this example:
    // - the DOGE/USD reference contracts uses 17 decimals
    // - on PBC we store DOGE as PUPPIES with 13 decimals, i.e. 1 DOGE = 1e13 PUPPIES

    // 1 DOGE = 1 USD
    Unsigned256 dogePriceWith17Decimals = Unsigned256.create(1_00000000000000000L); // 1e17
    Unsigned256 oneUsdInWei = calculateOneUsdInPuppies(dogePriceWith17Decimals);
    PricePublisher.ConversionRate conversionRate =
        PricePublisher.convert(dogePriceWith17Decimals, new Decimals(13, 17));
    Unsigned256 gas = externalCoinsToGas(oneUsdInWei, conversionRate);
    assertThat(gas).isEqualTo(Unsigned256.create(100_000L));
  }

  @Test
  void conversionRateGivesApproximatelyOneHundredThousandGasPerOneUsd() {
    Unsigned256 oneEthInUsdWithEightDecimals = Unsigned256.create(1286_52890554L);

    Unsigned256 oneUsdInWei = calculateOneUsdInWei(oneEthInUsdWithEightDecimals);

    PricePublisher.ConversionRate conversionRate =
        PricePublisher.convert(oneEthInUsdWithEightDecimals, new Decimals(18, 8));

    Unsigned256 gas = externalCoinsToGas(oneUsdInWei, conversionRate);
    assertThat(gas).isEqualTo(Unsigned256.create(99_999L));

    oneEthInUsdWithEightDecimals = Unsigned256.create(1542_47928122L);
    oneUsdInWei = calculateOneUsdInWei(oneEthInUsdWithEightDecimals);

    conversionRate = PricePublisher.convert(oneEthInUsdWithEightDecimals, new Decimals(18, 8));

    gas = externalCoinsToGas(oneUsdInWei, conversionRate);
    assertThat(gas).isEqualTo(Unsigned256.create(99_999L));
  }

  @Test
  void conversionRateGivesApproximatelyOneHundredThousandGasPerOneUsd_differentNumberOfDecimals() {
    // For this example:
    // - the DOGE/USD reference contracts uses 17 decimals
    // - on PBC we store DOGE as PUPPIES with 13 decimals, i.e. 1 DOGE = 1e13 PUPPIES

    // 1 DOGE = ~1286 USD
    Unsigned256 oneDogeInUsdWith17Decimals = Unsigned256.create("128652890554652890554");

    Unsigned256 oneUsdInPuppies = calculateOneUsdInPuppies(oneDogeInUsdWith17Decimals);

    PricePublisher.ConversionRate conversionRate =
        PricePublisher.convert(oneDogeInUsdWith17Decimals, new Decimals(13, 17));

    Unsigned256 gas = externalCoinsToGas(oneUsdInPuppies, conversionRate);
    assertThat(gas).isEqualTo(Unsigned256.create(99_999L));

    oneDogeInUsdWith17Decimals = Unsigned256.create("154247928122247928122");
    oneUsdInPuppies = calculateOneUsdInPuppies(oneDogeInUsdWith17Decimals);

    conversionRate = PricePublisher.convert(oneDogeInUsdWith17Decimals, new Decimals(13, 17));

    gas = externalCoinsToGas(oneUsdInPuppies, conversionRate);
    assertThat(gas).isEqualTo(Unsigned256.create(99_999L));
  }

  @Test
  void conversionRate1DollarEquals100000Gas() {
    PricePublisher.ConversionRate conversionRate =
        PricePublisher.convert(Unsigned256.TEN, new Decimals(1, 1));
    Unsigned256 gas = externalCoinsToGas(Unsigned256.TEN, conversionRate);
    assertThat(gas).isEqualTo(Unsigned256.create(100_000L));
  }

  @Test
  void truncate() {
    Unsigned256 largeNumber = Unsigned256.create(Long.MAX_VALUE);
    assertThat(PricePublisher.truncate(largeNumber, largeNumber))
        .isEqualTo(new PricePublisher.ConversionRate(Long.MAX_VALUE, Long.MAX_VALUE));

    // Long.MAX_VALUE * Long.MAX_VALUE
    Unsigned256 largerNumber = Unsigned256.create("85070591730234615847396907784232501249");
    assertThat(PricePublisher.truncate(largerNumber, largerNumber))
        .isEqualTo(new PricePublisher.ConversionRate(8507059173023461584L, 8507059173023461584L));

    Unsigned256 tooLargeNumerator = Unsigned256.create("21481698201819754993"); // 65 bit prime
    Unsigned256 tooLargeDenominator = Unsigned256.create("18598666868588881583"); // 65 bit prime

    PricePublisher.ConversionRate truncated =
        PricePublisher.truncate(tooLargeNumerator, tooLargeDenominator);

    // both numerator and denominator are truncated by a factor of 10
    assertThat(truncated.numerator()).isEqualTo(2148169820181975499L);
    assertThat(truncated.denominator()).isEqualTo(1859866686858888158L);
  }

  /**
   * Calculate the amount of wei corresponding to 1 USD from the price of 1 ETH in USD (with 8
   * decimals).
   *
   * <p>Since the price (<var>x</var>) is given with 8 decimals the conversion should be seen as:
   * <br>
   *
   * <pre>
   *   1e-8 <var>x</var> USD = 1 ETH
   *   1 USD = 1e8/<var>x</var> ETH
   * </pre>
   *
   * <p>The amount then needs to be converted from ETH to wei (1 ETH = 1e18 wei):
   *
   * <pre>
   *   1 USD = (1e8 * 1e18)/<var>x</var> wei
   * </pre>
   *
   * @param ethPrice USD price of 1 ETH
   * @return 1 USD worth of wei
   */
  private static Unsigned256 calculateOneUsdInWei(Unsigned256 ethPrice) {
    Unsigned256 oneUsdWith8Decimals = Unsigned256.create(1_00000000L); // 1e8
    Unsigned256 oneEthInWei = Unsigned256.create(1_000000000000000000L); // 1e18
    return oneUsdWith8Decimals.multiply(oneEthInWei).divide(ethPrice);
  }

  private static Unsigned256 calculateOneUsdInPuppies(Unsigned256 dogePrice) {
    Unsigned256 oneUsdWith17Decimals = Unsigned256.create(1_00000000000000000L); // 1e17
    Unsigned256 oneDogeInPuppies = Unsigned256.create(1_0000000000000L); // 1e13
    return oneUsdWith17Decimals.multiply(oneDogeInPuppies).divide(dogePrice);
  }

  private static Unsigned256 externalCoinsToGas(
      Unsigned256 ethAmount, PricePublisher.ConversionRate conversion) {
    return ethAmount
        .multiply(Unsigned256.create(conversion.numerator()))
        .divide(Unsigned256.create(conversion.denominator()));
  }
}
