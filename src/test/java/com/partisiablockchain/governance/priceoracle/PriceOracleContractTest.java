package com.partisiablockchain.governance.priceoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.ContractEventInteraction;
import com.partisiablockchain.contract.StateSerializableEquality;
import com.partisiablockchain.contract.SysContractContextTest;
import com.partisiablockchain.contract.SysContractSerialization;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test. */
final class PriceOracleContractTest {

  private static final BlockchainAddress accountA =
      BlockchainAddress.fromHash(
          BlockchainAddress.Type.ACCOUNT, Hash.create(h -> h.writeString("A")));
  private static final BlockchainAddress accountB =
      BlockchainAddress.fromHash(
          BlockchainAddress.Type.ACCOUNT, Hash.create(h -> h.writeString("B")));
  private static final BlockchainAddress accountC =
      BlockchainAddress.fromHash(
          BlockchainAddress.Type.ACCOUNT, Hash.create(h -> h.writeString("C")));
  private static final BlockchainAddress accountD =
      BlockchainAddress.fromHash(
          BlockchainAddress.Type.ACCOUNT, Hash.create(h -> h.writeString("D")));

  private static final BlockchainAddress largeOracleContractAddress =
      BlockchainAddress.fromHash(
          BlockchainAddress.Type.ACCOUNT,
          Hash.create(h -> h.writeString("largeOracleContractAddress")));

  private static final String referenceContractAddress = "referenceContractAddress";

  private static final String symbol = "MPC_ROPSTEN";
  private static final RoundDataRpc DUMMY_ROUND = testRound(1, 2, 0, 0, 1);
  private static final Decimals decimals = new Decimals(18, 8);

  private static final String chainId = "Polygon";

  private final SysContractSerialization<PriceOracleContractState> serialization =
      new SysContractSerialization<>(
          PriceOracleContractInvoker.class, PriceOracleContractState.class);
  private SysContractContextTest context;

  @BeforeEach
  void setup() {
    context = new SysContractContextTest(1, 100, accountA);
  }

  @Test
  void create() {
    PriceOracleContractState state =
        serialization.create(
            context,
            rpc -> {
              rpc.writeString(symbol);
              largeOracleContractAddress.write(rpc);
              rpc.writeString(referenceContractAddress);
              rpc.writeInt(18); // PBC decimals
              rpc.writeInt(8); // USD price decimals
              rpc.writeString(chainId);
            });
    assertThat(state).isNotNull();
    assertThat(state.getSymbol()).isEqualTo(symbol);
    assertThat(state.getLargeOracleContractAddress()).isEqualTo(largeOracleContractAddress);
    assertThat(state.getReferenceContractAddress()).isEqualTo(referenceContractAddress);
  }

  @Test
  void upgrade() {
    final PriceOracleContract contract = new PriceOracleContract();
    RoundDataRpc roundData = testRound(1, 2, 3, 4, 5);
    PriceOracleContractState state =
        createInitial().registerNode(accountA).addPriceUpdate(roundData.convert(), accountA);
    PriceOracleContractState newState = contract.upgrade(StateAccessor.create(state));
    StateSerializableEquality.assertStatesEqual(newState, state);

    state =
        state
            .startChallengePeriod(10, Unsigned256.ONE)
            .beginDispute(
                Dispute.create(roundData.convert(), roundData.convert(), context.getFrom()))
            .setQueuedDispute(
                Dispute.create(roundData.convert(), roundData.convert(), context.getFrom()));
    state = state.setNodeToOptedOut(accountA);
    newState = contract.upgrade(StateAccessor.create(state));
    StateSerializableEquality.assertStatesEqual(newState, state);
  }

  @Test
  void callback_deregister() {
    PriceOracleContractState state = registerAccounts(createInitial(), List.of(accountA));
    from(accountA);

    state = invokeDeregister(state);
    assertThat(state.isNodePending(accountA)).isTrue();
    state = callbackDeregister(state, accountA);
    assertThat(state.isNodePending(accountA)).isFalse();
    assertThat(state.isRegistered(accountA)).isFalse();
  }

  @Test
  void callback_deregister_failed() {
    PriceOracleContractState state = registerAccounts(createInitial(), List.of(accountA));
    from(accountA);
    state = invokeDeregister(state);
    assertThat(state.isNodePending(accountA)).isTrue();
    state = callbackDeregister(state, accountA, false);
    assertThat(state.isNodePending(accountA)).isFalse();
    assertThat(state.isRegistered(accountA)).isTrue();
  }

  @Test
  void callback_register() {
    PriceOracleContractState state = createInitial();
    from(accountA);
    state = invokeRegister(state);
    assertThat(state.isNodePending(accountA)).isTrue();
    state = callbackRegister(state, accountA);
    assertThat(state.isNodePending(accountA)).isFalse();
    assertThat(state.isRegistered(accountA)).isTrue();
  }

  @Test
  void callback_register_failed() {
    PriceOracleContractState state = createInitial();
    from(accountA);
    state = invokeRegister(state);
    assertThat(state.isNodePending(accountA)).isTrue();
    state = callbackRegister(state, accountA, false);
    assertThat(state.isNodePending(accountA)).isFalse();
    assertThat(state.isRegistered(accountA)).isFalse();
  }

  @Test
  void callback_startDispute() {
    PriceOracleContractState state =
        registerAccounts(createInitial(), List.of(accountA, accountB, accountC));
    RoundDataRpc roundData = testRound(1, 3, 0, 0, 1);
    state = startChallengePeriod(state, roundData, accountA, accountB, accountC);
    state = callbackStartDispute(state, accountD, roundData.convert(), true);
    assertThat(state.hasDispute()).isTrue();
    assertThat(state.getDispute().getOriginalClaim()).isEqualTo(roundData.convert());
    assertThat(state.getDispute().getChallenger()).isEqualTo(accountD);
  }

  @Test
  void callback_startDispute_failed() {
    final RoundDataRpc roundData = testRound(1, 3, 0, 0, 1);
    PriceOracleContractState state =
        registerAccounts(createInitial(), List.of(accountA, accountB, accountC));

    state = startChallengePeriod(state, roundData, accountA, accountB, accountC);

    from(accountD);
    state = invokeStartDispute(state, Unsigned256.create(roundData.roundId()));
    assertThat(state.hasPendingDispute()).isTrue();

    state = callbackStartDispute(state, accountD, roundData.convert(), false);
    assertThat(state.hasPendingDispute()).isFalse();
    assertThat(state.hasDispute()).isFalse();
  }

  @Test
  void callback_startDispute_failed_queuedDispute() {
    final RoundDataRpc roundData = testRound(1, 3, 0, 0, 1);
    final RoundDataRpc disputeRoundData = testRound(1, 4, 0, 0, 1);
    BlockchainAddress nonPriceOracleNode =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.ACCOUNT, Hash.create(h -> h.writeString("E")));
    PriceOracleContractState state =
        registerAccounts(createInitial(), List.of(accountA, accountB, accountC, accountD));

    state = startChallengePeriod(state, roundData, accountA, accountB, accountC);

    from(nonPriceOracleNode);
    state = invokeStartDispute(state, Unsigned256.create(roundData.roundId()));
    assertThat(state.hasPendingDispute()).isTrue();

    from(accountD);
    state = invokeNotifyPriceUpdate(disputeRoundData, state);
    Dispute queuedDispute = state.getQueuedDispute();

    state = callbackStartDispute(state, nonPriceOracleNode, roundData.convert(), false);
    assertThat(state.hasDispute()).isTrue();
    assertThat(state.getDispute().getClaims()).containsExactlyElementsOf(queuedDispute.getClaims());
    assertThat(state.getDispute().getChallenger()).isEqualTo(queuedDispute.getChallenger());
    ContractEventInteraction largeOracleCreateDisputeInvocation =
        (ContractEventInteraction) context.getInteractions().get(0);
    byte[] actualRpc = SafeDataOutputStream.serialize(largeOracleCreateDisputeInvocation.rpc);
    byte[] expectedRpc =
        SafeDataOutputStream.serialize(
            LargeOracleRpc.createDisputePoll(
                queuedDispute.getChallenger(), queuedDispute.getOriginalClaim().roundId()));
    assertThat(actualRpc).isEqualTo(expectedRpc);
  }

  @Test
  void invoke_deregister() {
    PriceOracleContractState state =
        registerAccounts(createInitial(), List.of(accountA, accountB, accountC, accountD));

    from(accountA);
    state = invokeDeregister(state);
    assertThat(state.isNodePending(accountA)).isTrue();

    ContractEventInteraction largeOracleDeregisterInvocation =
        (ContractEventInteraction) context.getRemoteCalls().contractEvents.get(0);
    byte[] actualRpc = SafeDataOutputStream.serialize(largeOracleDeregisterInvocation.rpc);
    byte[] expectedRpc =
        SafeDataOutputStream.serialize(LargeOracleRpc.unlockTokensFromPriceOracle(accountA));
    assertThat(actualRpc).isEqualTo(expectedRpc);

    byte[] actualCallbackRpc = context.getRemoteCalls().callbackRpc;
    byte[] expectedCallbackRpc =
        SafeDataOutputStream.serialize(PriceOracleContract.Callbacks.deregister(accountA));
    assertThat(actualCallbackRpc).isEqualTo(expectedCallbackRpc);
  }

  @Test
  void invoke_deregister_notRegistered() {
    PriceOracleContractState initial = createInitial();
    assertThat(initial.isRegistered(accountA)).isFalse();
    from(accountA);
    assertThatThrownBy(() -> invokeDeregister(initial))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only registered oracle nodes can be deregistered");
  }

  @Test
  void invoke_deregister_withPriceUpdates() {
    PriceOracleContractState state =
        registerAccounts(createInitial(), List.of(accountA, accountB, accountC, accountD));
    from(accountA);
    PriceOracleContractState priceUpdateState = invokeNotifyPriceUpdate(DUMMY_ROUND, state);
    assertThatThrownBy(() -> invokeDeregister(priceUpdateState))
        .isInstanceOf(RuntimeException.class)
        .hasMessage(
            "Cannot deregister an oracle node that has notified a price update on an ongoing"
                + " round");
  }

  @Test
  void invoke_deregister_withPriceUpdatesFromOtherAccount() {
    PriceOracleContractState state =
        registerAccounts(createInitial(), List.of(accountA, accountB, accountC, accountD));
    from(accountA);
    state = invokeNotifyPriceUpdate(DUMMY_ROUND, state);
    from(accountB);
    state = invokeDeregister(state);
    assertThat(state.isRegistered(accountB)).isFalse();
  }

  @Test
  void invoke_disputeCounterClaim() {
    PriceOracleContractState state = registerAccounts(createInitial(), List.of(accountA, accountB));
    RoundDataRpc roundData = testRound(1, 1, 1, 0, 1);
    RoundDataRpc disputingRoundData = testRound(1, 2, 1, 0, 1);

    state = startDispute(state, accountA, roundData, accountB, disputingRoundData);

    from(largeOracleContractAddress);
    RoundDataRpc counterClaim = testRound(1, 3, 0, 0, 1);
    state = invokeDisputeCounterClaim(state, counterClaim);
    assertThat(state.getDispute().getClaims()).hasSize(3);

    state = invokeDisputeCounterClaim(state, counterClaim);
    assertThat(state.getDispute().getClaims()).hasSize(3);
  }

  @Test
  void invoke_disputeCounterClaim_noDispute() {
    RoundDataRpc round = DUMMY_ROUND;
    PriceOracleContractState challengePeriodStarted =
        startChallengePeriod(
            registerAccounts(createInitial(), List.of(accountA, accountB, accountC, accountD)),
            round,
            accountA,
            accountB,
            accountC);
    from(accountD);
    PriceOracleContractState stateInDispute =
        invokeNotifyPriceUpdate(round, challengePeriodStarted);
    RoundDataRpc counterClaim = testRound(2, 3, 0, 0, 1);
    from(stateInDispute.getLargeOracleContractAddress());
    assertThat(stateInDispute.getDispute()).isNull();
    assertThatThrownBy(() -> invokeDisputeCounterClaim(stateInDispute, counterClaim))
        .hasMessage("No dispute to add counter-claim to");
  }

  @Test
  void invoke_disputeCounterClaim_notLargeOracleContract() {
    from(accountA);
    assertThatThrownBy(() -> invokeDisputeCounterClaim(createInitial(), testRound(1, 1, 1, 1, 1)))
        .hasMessage("Counter-claims can only be added through the large oracle contract");
  }

  @Test
  void publishPriceForEndedChallengePeriod() {
    final BlockchainAddress accountE =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.ACCOUNT, Hash.create(s -> s.writeString("accountE")));
    PriceOracleContractState state =
        registerAccounts(
            createInitial(), List.of(accountA, accountB, accountC, accountD, accountE));

    final RoundDataRpc firstRoundCorrect = testRound(1, 99, 0, 0, 1);
    final RoundDataRpc secondRound = testRound(2, 10, 0, 0, 2);
    final RoundDataRpc firstRoundIncorrect = testRound(1, 98, 0, 0, 1);

    from(accountA);
    state = invokeNotifyPriceUpdate(firstRoundCorrect, state);
    from(accountB);
    state = invokeNotifyPriceUpdate(secondRound, state);
    from(accountC);
    state = invokeNotifyPriceUpdate(secondRound, state);
    from(accountD);
    state = invokeNotifyPriceUpdate(secondRound, state);

    assertThat(state.getLatestPublishedRoundId()).isEqualTo(Unsigned256.ZERO);
    increaseBlockProductionTime(ChallengePeriod.CHALLENGE_PERIOD_DURATION_MILLIS + 1);

    from(accountE);
    state = invokeNotifyPriceUpdate(firstRoundIncorrect, state);

    assertThat(state.getDispute()).isNull();
    assertThat(state.getLatestPublishedRoundId()).isEqualTo(secondRound.convert().roundId());
  }

  @Test
  void startDisputeAfterPricePublish() {
    final BlockchainAddress accountE =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.ACCOUNT, Hash.create(s -> s.writeString("accountE")));
    PriceOracleContractState state =
        registerAccounts(
            createInitial(), List.of(accountA, accountB, accountC, accountD, accountE));

    final RoundDataRpc thirdRoundCorrect = testRound(3, 99, 0, 0, 3);
    final RoundDataRpc secondRound = testRound(2, 10, 0, 0, 2);
    final RoundDataRpc thirdRoundIncorrect = testRound(3, 98, 0, 0, 3);

    from(accountA);
    state = invokeNotifyPriceUpdate(thirdRoundCorrect, state);
    from(accountB);
    state = invokeNotifyPriceUpdate(secondRound, state);
    from(accountC);
    state = invokeNotifyPriceUpdate(secondRound, state);
    from(accountD);
    state = invokeNotifyPriceUpdate(secondRound, state);

    increaseBlockProductionTime(ChallengePeriod.CHALLENGE_PERIOD_DURATION_MILLIS + 1);
    assertThat(state.hasPendingDispute()).isFalse();

    from(accountE);
    state = invokeNotifyPriceUpdate(thirdRoundIncorrect, state);

    assertThat(state.getLatestPublishedRoundId()).isEqualTo(secondRound.convert().roundId());
    assertThat(state.hasPendingDispute()).isTrue();
  }

  @Test
  void startChallengePeriodAfterPricePublish() {
    final BlockchainAddress accountE =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.ACCOUNT, Hash.create(s -> s.writeString("accountE")));
    PriceOracleContractState state =
        registerAccounts(
            createInitial(), List.of(accountA, accountB, accountC, accountD, accountE));

    final RoundDataRpc thirdRound = testRound(3, 99, 0, 0, 3);
    final RoundDataRpc secondRound = testRound(2, 10, 0, 0, 2);

    from(accountA);
    state = invokeNotifyPriceUpdate(thirdRound, state);
    from(accountB);
    state = invokeNotifyPriceUpdate(thirdRound, state);
    state = invokeNotifyPriceUpdate(secondRound, state);
    from(accountC);
    state = invokeNotifyPriceUpdate(secondRound, state);
    from(accountD);
    state = invokeNotifyPriceUpdate(secondRound, state);

    increaseBlockProductionTime(ChallengePeriod.CHALLENGE_PERIOD_DURATION_MILLIS + 1);

    from(accountE);
    state = invokeNotifyPriceUpdate(thirdRound, state);

    assertThat(state.getLatestPublishedRoundId()).isEqualTo(secondRound.convert().roundId());
    assertThat(state.hasChallengePeriod()).isTrue();
  }

  @Test
  void invoke_disputeResult_honestAndMalicious() {
    int answer = 2_00000000;
    final RoundDataRpc incorrectRoundData = testRound(1, 1_00000000, 0, 0, 1);
    final RoundDataRpc incorrectFutureRoundData = testRound(2, 1_00000000, 0, 0, 2);
    final RoundDataRpc correctRoundData = testRound(1, answer, 0, 0, 1);
    final Unsigned256 roundId = incorrectRoundData.convert().roundId();

    BlockchainAddress accountE =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.ACCOUNT, Hash.create(h -> h.writeString("E")));
    PriceOracleContractState state =
        registerAccounts(
            createInitial(), List.of(accountA, accountB, accountC, accountD, accountE));
    from(accountD);
    state = invokeNotifyPriceUpdate(incorrectFutureRoundData, state);
    state = startChallengePeriod(state, incorrectRoundData, accountA, accountB, accountC);
    from(accountD);
    state = invokeNotifyPriceUpdate(incorrectRoundData, state);
    from(accountE);
    state = invokeNotifyPriceUpdate(correctRoundData, state);
    state =
        callbackStartDispute(
            state, accountE, incorrectRoundData.convert(), correctRoundData.convert(), true);
    int resultIndex = state.getDispute().getClaims().indexOf(correctRoundData.convert());
    assertThat(resultIndex).isEqualTo(1);

    from(largeOracleContractAddress);
    LargeOracleDisputeId largeOracleDisputeId = LargeOracleDisputeId.fromRoundId(roundId);
    PriceOracleContractState afterDisputeResult =
        invokeDisputeResult(
            state, largeOracleDisputeId.oracleId(), largeOracleDisputeId.disputeId(), resultIndex);
    assertThat(afterDisputeResult.getRegisteredOracleNodes()).containsExactly(accountE);
    assertThat(afterDisputeResult.hasDispute()).isFalse();
    assertThat(afterDisputeResult.hasChallengePeriod()).isFalse();
    assertThat(afterDisputeResult.hasPendingDispute()).isFalse();
    assertThat(afterDisputeResult.getPriceUpdatesForRound(roundId)).isNull();
    assertThat(afterDisputeResult.hasPriceUpdateFromNode(accountD)).isFalse();

    byte[] actualRpc = context.getUpdateGlobalAccountPluginState().getRpc();
    byte[] expectedRpc =
        PricePublisher.setCoin(
            symbol, PricePublisher.convert(Unsigned256.create(answer), decimals));
    assertThat(actualRpc).isEqualTo(expectedRpc);

    ContractEventInteraction largeOracleBurnTokensInvocation =
        (ContractEventInteraction) context.getInteractions().get(0);
    actualRpc = SafeDataOutputStream.serialize(largeOracleBurnTokensInvocation.rpc);
    expectedRpc =
        SafeDataOutputStream.serialize(
            LargeOracleRpc.burnStakedTokens(List.of(accountD, accountC, accountB, accountA)));
    assertThat(actualRpc).isEqualTo(expectedRpc);

    assertThat(context.getInfrastructureFeesGas())
        .isEqualTo(PricePublisher.GAS_REWARD_PER_PRICE_UPDATE);
    assertThat(context.getInfrastructureFeesTarget()).isEqualTo(accountE);
  }

  @Test
  void invoke_disputeResult_illegalRoundId() {
    final RoundDataRpc maliciousRoundData = testRound(9999, 0, 0, 0, 9999);
    final RoundDataRpc illegalRoundIdRoundData = testRound(9999, 0, 0, 0, 0);
    PriceOracleContractState state =
        registerAccounts(createInitial(), List.of(accountA, accountB, accountC, accountD));

    state = startChallengePeriod(state, maliciousRoundData, accountA, accountB, accountC);
    from(accountD);
    state = invokeStartDispute(state, Unsigned256.create(maliciousRoundData.roundId()));
    state = callbackStartDispute(state, accountD, maliciousRoundData.convert(), true);
    from(largeOracleContractAddress);
    state = invokeDisputeCounterClaim(state, illegalRoundIdRoundData);
    int resultIndex = state.getDispute().getClaims().indexOf(illegalRoundIdRoundData.convert());
    assertThat(resultIndex).isEqualTo(1);
    LargeOracleDisputeId largeOracleDisputeId =
        LargeOracleDisputeId.fromRoundId(illegalRoundIdRoundData.convert().roundId());
    state =
        invokeDisputeResult(
            state, largeOracleDisputeId.oracleId(), largeOracleDisputeId.disputeId(), resultIndex);
    assertThat(state.getRegisteredOracleNodes()).containsExactly(accountD);
    assertThat(state.hasDispute()).isFalse();
    assertThat(state.hasChallengePeriod()).isFalse();
    assertThat(state.hasPendingDispute()).isFalse();
    assertThat(state.getLatestPublishedRoundId()).isEqualTo(Unsigned256.ZERO);
    assertThat(state.getPriceUpdatesForRound(maliciousRoundData.convert().roundId())).isNull();

    ContractEventInteraction largeOracleBurnTokensInvocation =
        (ContractEventInteraction) context.getInteractions().get(0);
    byte[] actualRpc = SafeDataOutputStream.serialize(largeOracleBurnTokensInvocation.rpc);
    byte[] expectedRpc =
        SafeDataOutputStream.serialize(
            LargeOracleRpc.burnStakedTokens(List.of(accountC, accountB, accountA)));
    assertThat(actualRpc).isEqualTo(expectedRpc);
  }

  @Test
  void invoke_disputeResult_unknownRoundId() {
    RoundDataRpc roundData = testRound(1, 1, 0, 0, 1);
    RoundDataRpc disputingRoundData = testRound(1, 2, 0, 0, 1);
    PriceOracleContractState state = registerAccounts(createInitial(), List.of(accountA, accountB));
    PriceOracleContractState disputeState =
        startDispute(state, accountA, roundData, accountB, disputingRoundData);
    from(largeOracleContractAddress);
    assertThatThrownBy(() -> invokeDisputeResult(disputeState, 99, 99, 0))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Dispute result was for another dispute");
  }

  @Test
  void invoke_disputeResult_noDispute() {
    PriceOracleContractState state = createInitial();
    from(largeOracleContractAddress);
    assertThatThrownBy(() -> invokeDisputeResult(state, 0, 1, 0))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Dispute does not exist");
  }

  @Test
  void invoke_disputeResult_noFraud() {
    PriceOracleContractState state = registerAccounts(createInitial(), List.of(accountA, accountB));
    RoundDataRpc roundData = testRound(1, 1, 1, 0, 1);
    RoundDataRpc disputingRoundData = testRound(1, 2, 1, 0, 1);

    state = startDispute(state, accountA, roundData, accountB, disputingRoundData);

    LargeOracleDisputeId largeOracleDisputeId =
        LargeOracleDisputeId.fromRoundId(roundData.convert().roundId());
    from(largeOracleContractAddress);
    state =
        invokeDisputeResult(
            state,
            largeOracleDisputeId.oracleId(),
            largeOracleDisputeId.disputeId(),
            PriceOracleContract.NO_FRAUD);
    assertThat(state.hasChallengePeriod()).isFalse();
  }

  private PriceOracleContractState startDispute(
      PriceOracleContractState state,
      BlockchainAddress accountA,
      RoundDataRpc roundData,
      BlockchainAddress accountB,
      RoundDataRpc disputingRoundData) {
    from(accountA);
    state = invokeNotifyPriceUpdate(roundData, state);
    from(accountB);
    state = invokeNotifyPriceUpdate(disputingRoundData, state);
    assertThat(state.hasPendingDispute()).isTrue();
    state =
        callbackStartDispute(
            state, accountB, roundData.convert(), disputingRoundData.convert(), true);
    assertThat(state.hasDispute()).isTrue();
    assertThat(state.getDispute().getOriginalClaim()).isEqualTo(roundData.convert());
    assertThat(state.getDispute().getClaims().get(1)).isEqualTo(disputingRoundData.convert());
    assertThat(state.getDispute().getChallenger()).isEqualTo(accountB);
    return state;
  }

  @Test
  void invoke_disputeResult_notLargeOracleContract() {
    PriceOracleContractState state =
        startChallengePeriod(
            registerAccounts(createInitial(), List.of(accountA, accountB, accountC, accountD)),
            DUMMY_ROUND,
            accountA,
            accountB,
            accountC);
    LargeOracleDisputeId largeOracleDisputeId =
        LargeOracleDisputeId.fromRoundId(DUMMY_ROUND.convert().roundId());
    from(accountA);
    assertThatThrownBy(
            () ->
                invokeDisputeResult(
                    state, largeOracleDisputeId.oracleId(), largeOracleDisputeId.disputeId(), 0))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Dispute can only be resolved by large oracle contract");
  }

  @Test
  void invoke_notifyPriceUpdate() {
    final int roundId = 1;
    final int answer = 2;
    final int startedAt = 3;
    final int updatedAt = 4;
    final int answeredInRound = 5;

    RoundDataRpc roundData = testRound(roundId, answer, startedAt, updatedAt, answeredInRound);
    increaseBlockProductionTimeSeconds(updatedAt);
    PriceOracleContractState state = createInitial().registerNode(accountA).registerNode(accountB);
    state = invokeNotifyPriceUpdate(roundData, state);
    RoundData notifiedRoundData =
        state.getPriceUpdatesForRound(roundData.convert().roundId()).getPriceUpdate(accountA);
    assertThat(notifiedRoundData.roundId()).isEqualTo(Unsigned256.create(roundId));
    assertThat(notifiedRoundData.answer()).isEqualTo(Unsigned256.create(answer));
    assertThat(notifiedRoundData.startedAt()).isEqualTo(Unsigned256.create(startedAt));
    assertThat(notifiedRoundData.updatedAt()).isEqualTo(Unsigned256.create(updatedAt));
    assertThat(notifiedRoundData.answeredInRound()).isEqualTo(Unsigned256.create(answeredInRound));

    PriceUpdates roundPriceUpdates = state.getPriceUpdatesForRound(roundData.convert().roundId());
    assertThat(roundPriceUpdates).isNotNull();
    StateSerializableEquality.assertStatesEqual(
        roundPriceUpdates.getPriceUpdate(accountA), roundData.convert());
    assertThat(roundPriceUpdates.determinePrice()).isEqualTo(roundData.answer());
    assertThat(state.hasChallengePeriod()).isFalse();

    from(accountB);
    state = invokeNotifyPriceUpdate(roundData, state);
    assertThat(state.hasChallengePeriod()).isFalse();
  }

  @Test
  void invoke_notifyPriceUpdate_atExactTime() {
    from(accountA);
    assertThat(context.getBlockProductionTime()).isEqualTo(1);
    long updatedAt = 100;
    // Convert updatedAt to block production time format (seconds -> milliseconds)
    increaseBlockProductionTime(updatedAt * 1000 - 1);
    assertThat(context.getBlockProductionTime()).isEqualTo(updatedAt * 1000);
    PriceOracleContractState registeredState =
        registerAccounts(createInitial(), List.of(accountA, accountB, accountC, accountD));
    RoundDataRpc originalAnswer = testRound(1, 10, 0, updatedAt, 2);
    PriceOracleContractState notifyPriceUpdate =
        invokeNotifyPriceUpdate(originalAnswer, registeredState);
    assertThat(notifyPriceUpdate.getPriceUpdatesForRound(originalAnswer.convert().roundId()))
        .isNotNull();
  }

  @Test
  void invoke_notifyPriceUpdate_illegalRoundIdRoundDataDuringChallengePeriod() {
    final RoundDataRpc maliciousRoundData = testRound(9999, 0, 0, 0, 9999);
    final RoundDataRpc illegalRoundIdRoundData = testRound(9999, 0, 0, 0, 0);
    PriceOracleContractState state =
        registerAccounts(createInitial(), List.of(accountA, accountB, accountC, accountD));
    state = startChallengePeriod(state, maliciousRoundData, accountA, accountB, accountC);
    from(accountD);
    state = invokeNotifyPriceUpdate(illegalRoundIdRoundData, state);
    assertThat(state.hasPendingDispute()).isTrue();
  }

  @Test
  void invoke_notifyPriceUpdate_invalid_illegalRoundIdRoundData() {
    PriceOracleContractState state =
        registerAccounts(createInitial(), List.of(accountA, accountB, accountC, accountD));
    RoundDataRpc illegalRoundIdRoundData = testRound(9999, 0, 0, 0, 0);
    from(accountA);
    assertThatThrownBy(() -> invokeNotifyPriceUpdate(illegalRoundIdRoundData, state))
        .isInstanceOf(RuntimeException.class)
        .hasMessage(
            "It is only allowed to notify the special \"illegal round id\" round data during a"
                + " challenge period");

    // Additional coverage
    RoundDataRpc normalRoundData = testRound(9999, 1, 0, 0, 0);
    assertThat(normalRoundData.convert().isIllegalRoundIdRoundData()).isFalse();
    normalRoundData = testRound(9999, 0, 1, 0, 0);
    assertThat(normalRoundData.convert().isIllegalRoundIdRoundData()).isFalse();
    normalRoundData = testRound(9999, 0, 0, 1, 0);
    assertThat(normalRoundData.convert().isIllegalRoundIdRoundData()).isFalse();
    normalRoundData = testRound(9999, 0, 0, 0, 1);
    assertThat(normalRoundData.convert().isIllegalRoundIdRoundData()).isFalse();
  }

  @Test
  void invoke_notifyPriceUpdate_invalid_notOracleNode() {
    RoundDataRpc roundData = testRound(1, 3, 0, 0, 2);
    from(accountA);
    PriceOracleContractState initial = createInitial();
    assertThat(initial.getRegisteredOracleNodes()).doesNotContain(accountA);
    assertThatThrownBy(() -> invokeNotifyPriceUpdate(roundData, initial))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only oracle members can notify price updates");
  }

  @Test
  void invoke_notifyPriceUpdate_invalid_ongoingDispute() {
    RoundDataRpc roundData = testRound(1, 3, 0, 0, 2);
    RoundDataRpc disputingRoundData = testRound(1, 4, 0, 0, 2);
    from(accountA);
    PriceOracleContractState state =
        registerAccounts(createInitial(), List.of(accountA, accountB, accountC));
    PriceOracleContractState disputeState =
        startDispute(state, accountA, roundData, accountB, disputingRoundData);
    from(accountC);
    assertThatThrownBy(() -> invokeNotifyPriceUpdate(roundData, disputeState))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Price updates cannot be notified while a dispute is ongoing");
  }

  @Test
  void invoke_notifyPriceUpdate_invalid_tooOldRound() {
    final RoundDataRpc latest = testRound(2, 2, 0, 0, 1);
    final RoundDataRpc old = testRound(1, 3, 0, 0, 2);

    PriceOracleContractState challengePeriodStarted =
        startChallengePeriod(
            registerAccounts(createInitial(), List.of(accountA, accountB, accountC, accountD)),
            latest,
            accountA,
            accountB,
            accountC);

    increaseBlockProductionTime(ChallengePeriod.CHALLENGE_PERIOD_DURATION_MILLIS + 1);
    from(accountD);
    PriceOracleContractState challengePeriodOver =
        invokeNotifyPriceUpdate(latest, challengePeriodStarted);

    increaseBlockProductionTime(ChallengePeriod.CHALLENGE_PERIOD_DURATION_MILLIS + 1);

    // old after challenge period is over
    from(accountA);
    assertThatThrownBy(() -> invokeNotifyPriceUpdate(old, challengePeriodOver))
        .isInstanceOf(RuntimeException.class)
        .hasMessage(
            "Only price updates for rounds newer than the latest published round are allowed");

    // same as latest after challenge period is over
    from(accountA);
    assertThatThrownBy(() -> invokeNotifyPriceUpdate(latest, challengePeriodOver))
        .isInstanceOf(RuntimeException.class)
        .hasMessage(
            "Only price updates for rounds newer than the latest published round are allowed");
  }

  @Test
  void invoke_notifyPriceUpdate_invalid_nonHistoricRoundData() {
    long updatedAt = 99;
    RoundDataRpc originalAnswer = testRound(1, 10, 0, updatedAt + 1, 2);
    increaseBlockProductionTimeSeconds(updatedAt);
    PriceOracleContractState registeredState =
        registerAccounts(createInitial(), List.of(accountA, accountB, accountC, accountD));
    from(accountA);
    assertThatThrownBy(() -> invokeNotifyPriceUpdate(originalAnswer, registeredState))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Round data is after block production time");
  }

  @Test
  void invoke_notifyPriceUpdate_otherRoundThanChallengePeriod() {
    final RoundDataRpc current = testRound(2, 2, 0, 0, 1);

    PriceOracleContractState challengePeriodStarted =
        startChallengePeriod(
            registerAccounts(createInitial(), List.of(accountA, accountB, accountC, accountD)),
            current,
            accountA,
            accountB,
            accountC);

    from(accountD);
    assertThat(challengePeriodStarted.getChallengePeriod().roundId())
        .isEqualTo(Unsigned256.create(2));
    assertThatThrownBy(
            () -> invokeNotifyPriceUpdate(testRound(3, 2, 0, 0, 0), challengePeriodStarted))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only price updates for the round of the ongoing challenge period are allowed");
  }

  @Test
  void invoke_notifyPriceUpdate_triggerChallengePeriod() {
    RoundDataRpc round = DUMMY_ROUND;
    PriceOracleContractState challengePeriodStarted =
        startChallengePeriod(
            registerAccounts(createInitial(), List.of(accountA, accountB, accountC, accountD)),
            round,
            accountA,
            accountB,
            accountC);
    ChallengePeriod challengePeriod = challengePeriodStarted.getChallengePeriod();
    assertThat(challengePeriod.roundId()).isEqualTo(round.convert().roundId());
    assertThat(challengePeriod.startedAt()).isEqualTo(context.getBlockProductionTime());

    // End challenge period
    increaseBlockProductionTime(ChallengePeriod.CHALLENGE_PERIOD_DURATION_MILLIS + 1);
    from(accountD);
    invokeNotifyPriceUpdate(testRound(2, 2, 0, 0, 1), challengePeriodStarted);

    assertThat(context.getInfrastructureFeesGas())
        .isEqualTo(PricePublisher.GAS_REWARD_PER_PRICE_UPDATE / 3 + 1);

    PricePublisher.ConversionRate conversionRate = PricePublisher.convert(round.answer(), decimals);
    assertThat(context.getUpdateGlobalAccountPluginState().getRpc())
        .isEqualTo(PricePublisher.setCoin(symbol, conversionRate));
  }

  @Test
  void invoke_notifyPriceUpdate_triggerChallengePeriod_shouldNotReset() {
    RoundDataRpc round = DUMMY_ROUND;
    PriceOracleContractState challengePeriodStarted =
        startChallengePeriod(
            registerAccounts(createInitial(), List.of(accountA, accountB, accountC, accountD)),
            round,
            accountA,
            accountB,
            accountC);
    long expectedChallengePeriodEnd =
        context.getBlockProductionTime() + ChallengePeriod.CHALLENGE_PERIOD_DURATION_MILLIS + 1;
    assertThat(challengePeriodStarted.getChallengePeriod().hasEnded(expectedChallengePeriodEnd - 1))
        .isFalse();
    assertThat(challengePeriodStarted.getChallengePeriod().hasEnded(expectedChallengePeriodEnd))
        .isTrue();

    from(accountD);
    PriceOracleContractState additionalPriceUpdateForRoundWithChallengePeriod =
        invokeNotifyPriceUpdate(round, challengePeriodStarted);
    assertThat(
            additionalPriceUpdateForRoundWithChallengePeriod
                .getChallengePeriod()
                .hasEnded(expectedChallengePeriodEnd))
        .isTrue();
  }

  @Test
  void invoke_notifyPriceUpdate_triggerPublishPrice_cleanOldPriceUpdates() {
    final RoundDataRpc old = testRound(2, 2, 0, 0, 2);
    final RoundDataRpc current = testRound(3, 3, 0, 0, 3);
    final RoundDataRpc future = testRound(4, 2, 0, 0, 4);

    PriceOracleContractState accountsRegistered =
        registerAccounts(createInitial(), List.of(accountA, accountB, accountC, accountD));
    from(accountA);
    PriceOracleContractState oldFromA = invokeNotifyPriceUpdate(old, accountsRegistered);
    PriceOracleContractState futureFromA = invokeNotifyPriceUpdate(future, oldFromA);

    PriceOracleContractState challengePeriodStarted =
        startChallengePeriod(futureFromA, current, accountA, accountB, accountC);
    assertThat(challengePeriodStarted.getPriceUpdatesForRound(old.convert().roundId())).isNotNull();
    assertThat(challengePeriodStarted.getPriceUpdatesForRound(current.convert().roundId()))
        .isNotNull();
    assertThat(challengePeriodStarted.getPriceUpdatesForRound(future.convert().roundId()))
        .isNotNull();

    increaseBlockProductionTime(ChallengePeriod.CHALLENGE_PERIOD_DURATION_MILLIS + 1);
    from(accountD);
    PriceOracleContractState challengePeriodOver =
        invokeNotifyPriceUpdate(future, challengePeriodStarted);

    assertThat(challengePeriodOver.getLatestPublishedRoundId())
        .isEqualTo(current.convert().roundId());
    assertThat(challengePeriodOver.getPriceUpdatesForRound(old.convert().roundId())).isNull();
    assertThat(challengePeriodOver.getPriceUpdatesForRound(current.convert().roundId())).isNull();
    assertThat(challengePeriodOver.getPriceUpdatesForRound(future.convert().roundId())).isNotNull();
  }

  @Test
  void invoke_notifyPriceUpdate_triggerPublishPrice_ignoreIfPendingDispute() {
    final BlockchainAddress accountE =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.ACCOUNT, Hash.create(h -> h.writeString("E")));
    final RoundDataRpc roundData = testRound(3, 3, 0, 0, 3);

    PriceOracleContractState state =
        registerAccounts(createInitial(), List.of(accountA, accountB, accountC, accountD));
    state = startChallengePeriod(state, roundData, accountA, accountB, accountC);

    from(accountE);
    state = invokeStartDispute(state, Unsigned256.create(roundData.roundId()));

    increaseBlockProductionTime(ChallengePeriod.CHALLENGE_PERIOD_DURATION_MILLIS + 1);

    from(accountD);
    state = invokeNotifyPriceUpdate(roundData, state);
    assertThat(state.getLatestPublishedRoundId()).isEqualTo(Unsigned256.ZERO);
    assertThat(context.getUpdateGlobalAccountPluginState()).isNull();
  }

  @Test
  void invoke_notifyPriceUpdate_triggerDispute() {
    final RoundDataRpc roundData = testRound(1, 1, 0, 0, 1);
    final RoundDataRpc disputingRoundData = testRound(1, 2, 0, 0, 1);

    PriceOracleContractState state = registerAccounts(createInitial(), List.of(accountA, accountB));
    from(accountA);
    state = invokeNotifyPriceUpdate(roundData, state);
    from(accountB);
    invokeNotifyPriceUpdate(disputingRoundData, state);

    ContractEventInteraction largeOracleCreateDisputeInvocation =
        (ContractEventInteraction) context.getRemoteCalls().contractEvents.get(0);
    byte[] actualRpc = SafeDataOutputStream.serialize(largeOracleCreateDisputeInvocation.rpc);
    byte[] expectedRpc =
        SafeDataOutputStream.serialize(
            LargeOracleRpc.createDisputePoll(accountB, roundData.convert().roundId()));
    assertThat(actualRpc).isEqualTo(expectedRpc);

    byte[] actualCallbackRpc = context.getRemoteCalls().callbackRpc;
    byte[] expectedCallbackRpc =
        SafeDataOutputStream.serialize(
            PriceOracleContract.Callbacks.startDispute(
                accountB, roundData.convert(), disputingRoundData.convert()));
    assertThat(actualCallbackRpc).isEqualTo(expectedCallbackRpc);
  }

  @Test
  void invoke_register() {
    from(accountA);
    PriceOracleContractState state = invokeRegister(createInitial());

    ContractEventInteraction largeOracleRegisterInvocation =
        (ContractEventInteraction) context.getRemoteCalls().contractEvents.get(0);
    byte[] actualRpc = SafeDataOutputStream.serialize(largeOracleRegisterInvocation.rpc);
    byte[] expectedRpc =
        SafeDataOutputStream.serialize(LargeOracleRpc.lockTokensToPriceOracle(accountA));
    assertThat(actualRpc).isEqualTo(expectedRpc);

    byte[] actualCallbackRpc = context.getRemoteCalls().callbackRpc;
    byte[] expectedCallbackRpc =
        SafeDataOutputStream.serialize(PriceOracleContract.Callbacks.register(context.getFrom()));
    assertThat(actualCallbackRpc).isEqualTo(expectedCallbackRpc);
    assertThat(state.getRegisteredOracleNodes()).isEmpty();
  }

  @Test
  void invoke_register_alreadyRegistered() {
    PriceOracleContractState state =
        registerAccounts(createInitial(), List.of(accountA, accountB, accountC, accountD));
    assertThat(state.getRegisteredOracleNodes()).contains(accountA);
    from(accountA);
    assertThatThrownBy(() -> invokeRegister(state))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Cannot register an account that has already been registered");
  }

  @Test
  void invoke_register_pending() {
    from(accountA);
    PriceOracleContractState pendingDeregister = invokeRegister(createInitial());
    assertThatThrownBy(() -> invokeRegister(pendingDeregister))
        .isInstanceOf(RuntimeException.class)
        .hasMessage(
            "Cannot register while waiting on response from another register or deregister");
  }

  @Test
  void invoke_startDispute_nonPriceOracle() {
    PriceOracleContractState state =
        registerAccounts(createInitial(), List.of(accountA, accountB, accountC));
    RoundDataRpc roundData = DUMMY_ROUND;
    state = startChallengePeriod(state, roundData, accountA, accountB, accountC);
    Unsigned256 roundId = roundData.convert().roundId();

    from(accountD);
    state = invokeStartDispute(state, Unsigned256.create(roundData.roundId()));
    assertThat(state.hasPendingDispute()).isTrue();

    ContractEventInteraction largeOracleCreateDisputeInvocation =
        (ContractEventInteraction) context.getRemoteCalls().contractEvents.get(0);
    byte[] actualRpc = SafeDataOutputStream.serialize(largeOracleCreateDisputeInvocation.rpc);
    byte[] expectedRpc =
        SafeDataOutputStream.serialize(
            LargeOracleRpc.createDisputePoll(
                accountD, roundId, LargeOracleRpc.REGISTER_STAKED_TOKENS));
    assertThat(actualRpc).isEqualTo(expectedRpc);

    byte[] actualCallbackRpc = context.getRemoteCalls().callbackRpc;
    byte[] expectedCallbackRpc =
        SafeDataOutputStream.serialize(
            PriceOracleContract.Callbacks.startDispute(accountD, roundData.convert()));
    assertThat(actualCallbackRpc).isEqualTo(expectedCallbackRpc);
  }

  @Test
  void invoke_startDispute_noChallengePeriod() {
    from(accountD);
    assertThatThrownBy(() -> invokeStartDispute(createInitial(), Unsigned256.create(new byte[1])))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("There is not an active challenge period for this round id");
  }

  @Test
  void invoke_startDispute_challengePeriodIsOver() {
    PriceOracleContractState state =
        registerAccounts(createInitial(), List.of(accountA, accountB, accountC));
    RoundDataRpc roundDataA = testRound(1, 2, 0, 0, 1);
    PriceOracleContractState withChallengePeriod =
        startChallengePeriod(state, roundDataA, accountA, accountB, accountC);
    increaseBlockProductionTimeSeconds(ChallengePeriod.CHALLENGE_PERIOD_DURATION_MILLIS);
    from(accountD);
    byte[] roundInBytes = SafeDataOutputStream.serialize(s -> s.writeLong(1));
    assertThatThrownBy(
            () -> invokeStartDispute(withChallengePeriod, Unsigned256.create(roundInBytes)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("A dispute can not be started for a challenge period that is over");
  }

  @Test
  void invoke_startDispute_challengePeriodForOtherRoundId() {
    PriceOracleContractState state =
        registerAccounts(createInitial(), List.of(accountA, accountB, accountC));
    RoundDataRpc roundDataA = testRound(2, 2, 0, 0, 1);
    PriceOracleContractState withChallengePeriod =
        startChallengePeriod(state, roundDataA, accountA, accountB, accountC);
    from(accountD);
    assertThatThrownBy(
            () -> invokeStartDispute(withChallengePeriod, Unsigned256.create(new byte[5])))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("There is not an active challenge period for this round id");
  }

  @Test
  void invoke_startDispute_ongoingDispute() {
    PriceOracleContractState state = registerAccounts(createInitial(), List.of(accountA, accountB));
    RoundDataRpc roundData = testRound(1, 1, 1, 0, 1);
    RoundDataRpc disputingRoundData = testRound(1, 2, 1, 0, 1);
    PriceOracleContractState stateWithDispute =
        startDispute(state, accountA, roundData, accountB, disputingRoundData);
    assertThatThrownBy(() -> invokeStartDispute(stateWithDispute, Unsigned256.create(new byte[3])))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("There is already an ongoing dispute");
  }

  @Test
  void invoke_startDispute_pendingDispute() {
    PriceOracleContractState state =
        registerAccounts(createInitial(), List.of(accountA, accountB, accountC, accountD));
    PriceOracleContractState startChallengePeriod =
        startChallengePeriod(state, DUMMY_ROUND, accountA, accountB, accountC);
    byte[] roundOne = SafeDataOutputStream.serialize(s -> s.writeLong(1));
    PriceOracleContractState updatedState =
        invokeStartDispute(startChallengePeriod, Unsigned256.create(roundOne));
    assertThatThrownBy(() -> invokeStartDispute(updatedState, Unsigned256.create(roundOne)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("There is already a dispute pending for this round");
  }

  /**
   * If a node opts out, the contract registers it as opted out. If the node opts in again, it is no
   * longer registered as opted out.
   */
  @Test
  void invoke_optOutAndIn() {
    PriceOracleContractState state =
        registerAccounts(createInitial(), List.of(accountA, accountB, accountC, accountD));
    from(accountA);
    state = invokeOptOut(state);
    assertThat(state.isNodeOptedOut(accountA)).isTrue();
    state = invokeOptIn(state);
    assertThat(state.isNodeOptedOut(accountA)).isFalse();
  }

  /**
   * If a node opts out without being registered, or opts out twice in a row, an error is thrown.
   */
  @Test
  void optOutFailsIfNotRegisteredOrAlreadyOptedOut() {
    PriceOracleContractState state =
        registerAccounts(createInitial(), List.of(accountB, accountC, accountD));
    from(accountA);
    assertThatThrownBy(() -> invokeOptOut(state))
        .hasMessageContaining("Only registered nodes can opt out");
    PriceOracleContractState accountRegistered = registerAccounts(state, List.of(accountA));
    PriceOracleContractState accountOptedOut = invokeOptOut(accountRegistered);
    // Account a has status OPTED_OUT and can therefore not opt out again
    assertThatThrownBy(() -> invokeOptOut(accountOptedOut))
        .hasMessage("Only registered nodes can opt out");
  }

  /** A node can not opt in if it is not already opted out. */
  @Test
  void optInFailsIfNotOptedOut() {
    PriceOracleContractState state =
        registerAccounts(createInitial(), List.of(accountB, accountC, accountD));
    // Node can not opt in if they are not registered
    from(accountA);
    assertThatThrownBy(() -> invokeOptIn(state))
        .hasMessage("Only nodes that have opted out can opt in");
    // Node can not opt in if they are registered but not previously opted out
    PriceOracleContractState stateWithAccountRegistered =
        registerAccounts(state, List.of(accountA));
    assertThatThrownBy(() -> invokeOptIn(stateWithAccountRegistered))
        .hasMessage("Only nodes that have opted out can opt in");
  }

  /** Node can not register when they are opted out. */
  @Test
  void canNotRegisterIfOptedOut() {
    PriceOracleContractState state =
        registerAccounts(createInitial(), List.of(accountA, accountB, accountC, accountD));
    from(accountA);
    PriceOracleContractState accountOptsOutState = invokeOptOut(state);
    assertThatThrownBy(() -> invokeRegister(accountOptsOutState))
        .hasMessage("Cannot register an account that has already been registered");
  }

  /**
   * If a node opts out, the contract ignores price updates from this node. If the node opts in
   * again, the contract handles price updates from this node again.
   */
  @Test
  void optOutIgnoresNode() {
    PriceOracleContractState state =
        registerAccounts(createInitial(), List.of(accountA, accountB, accountC));
    RoundDataRpc roundData = testRound(2, 2, 0, 0, 1);
    from(accountA);
    state = invokeOptOut(state);
    PriceOracleContractState finalState = state;
    assertThatThrownBy(() -> invokeNotifyPriceUpdate(roundData, finalState))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining(
            "Price updates can not be notified by a node that has opted out of notifying");
    from(accountB);
    state = invokeNotifyPriceUpdate(roundData, state);
    from(accountC);
    state = invokeNotifyPriceUpdate(roundData, state);
    // Since accountA is ignored, there should only be two updates
    List<BlockchainAddress> notifyingNodes =
        state.getPriceUpdatesForRound(Unsigned256.create(2)).getNotifyingOracleNodes();
    assertThat(notifyingNodes).doesNotContain(accountA);
    assertThat(notifyingNodes).hasSize(2);

    // Opt-in, notify update and ensure update is registered
    from(accountA);
    state = invokeOptIn(state);
    state = invokeNotifyPriceUpdate(roundData, state);
    notifyingNodes = state.getPriceUpdatesForRound(Unsigned256.create(2)).getNotifyingOracleNodes();
    assertThat(notifyingNodes).contains(accountA);
    assertThat(notifyingNodes).hasSize(3);
  }

  /** If a node that has status OPTED_OUT deregisters, their status is no longer OPTED_OUT. */
  @Test
  void deregisterRemovesOptedOut() {
    PriceOracleContractState state =
        registerAccounts(createInitial(), List.of(accountA, accountB, accountC));
    from(accountA);
    state = invokeOptOut(state);
    assertThat(state.isNodeOptedOut(accountA)).isTrue();
    state = invokeDeregister(state);
    state = callbackDeregister(state, accountA);
    assertThat(state.isNodeOptedOut(accountA)).isFalse();
  }

  /** A node can only notify a price update once. */
  @Test
  void notifyPriceUpdateTwice() {
    PriceOracleContractState initialState =
        registerAccounts(createInitial(), List.of(accountA, accountB, accountC, accountD));
    final RoundDataRpc firstPrice = testRound(1, 1, 0, 0, 1);
    final RoundDataRpc secondPrice = testRound(1, 2, 0, 0, 1);

    from(accountA);
    PriceOracleContractState state = invokeNotifyPriceUpdate(firstPrice, initialState);
    assertThatThrownBy(() -> invokeNotifyPriceUpdate(secondPrice, state))
        .hasMessage("Price updates can not be notified more than once");
  }

  /**
   * If a node sends a new price for the round of the finished challenge then this price is ignored
   * and the price of the challenge is published.
   */
  @Test
  void triggerPublishPriceWithNewPriceNotification() {
    final long correctPrice = 330551000003L;
    final long incorrectPrice = 330451000003L;
    PriceOracleContractState state =
        registerAccounts(createInitial(), List.of(accountA, accountB, accountC));
    RoundDataRpc originalRoundData = testRound(1, correctPrice, 0, 0, 1);
    state = startChallengePeriod(state, originalRoundData, accountA, accountB, accountC);
    increaseBlockProductionTime(ChallengePeriod.CHALLENGE_PERIOD_DURATION_MILLIS + 1);
    assertThat(state.hasChallengePeriod()).isTrue();
    assertThat(state.isChallengePeriodOver(context.getBlockProductionTime())).isTrue();

    from(accountC);
    RoundDataRpc newRoundData = testRound(1, incorrectPrice, 0, 0, 1);
    state = invokeNotifyPriceUpdate(newRoundData, state);
    assertThat(state.hasChallengePeriod()).isFalse();
    assertThat(state.isChallengePeriodOver(context.getBlockProductionTime())).isFalse();
    assertThat(state.getLatestPublishedRoundId()).isEqualTo(Unsigned256.ONE);
    assertThat(state.getPriceUpdatesForRound(Unsigned256.ONE)).isNull();
    byte[] actualRpc = context.getUpdateGlobalAccountPluginState().getRpc();
    byte[] expectedRpc =
        PricePublisher.setCoin(
            symbol, PricePublisher.convert(Unsigned256.create(correctPrice), decimals));
    assertThat(actualRpc).isEqualTo(expectedRpc);
  }

  /**
   * If there is a finished challenge period and a node sends a price update for a newer round, then
   * the price is published and the price update is saved.
   */
  @Test
  void publishAndNotifyPriceUpdate() {
    final long updatePrice = 330451000003L;
    final RoundDataRpc originalRoundData = testRound(1, 330551000003L, 0, 0, 1);
    final RoundDataRpc newRoundData = testRound(2, updatePrice, 0, 0, 1);

    PriceOracleContractState state =
        registerAccounts(createInitial(), List.of(accountA, accountB, accountC));
    state = startChallengePeriod(state, originalRoundData, accountA, accountB, accountC);
    increaseBlockProductionTime(ChallengePeriod.CHALLENGE_PERIOD_DURATION_MILLIS + 1);
    assertThat(state.hasChallengePeriod()).isTrue();
    assertThat(state.isChallengePeriodOver(context.getBlockProductionTime())).isTrue();

    from(accountA);
    state = invokeNotifyPriceUpdate(newRoundData, state);
    assertThat(state.hasChallengePeriod()).isFalse();
    assertThat(state.isChallengePeriodOver(context.getBlockProductionTime())).isFalse();
    assertThat(state.getLatestPublishedRoundId()).isEqualTo(Unsigned256.ONE);
    RoundData priceUpdate =
        state.getPriceUpdatesForRound(Unsigned256.create(2)).getPriceUpdate(accountA);
    assertThat(priceUpdate).isEqualTo(newRoundData.convert());
  }

  private PriceOracleContractState callbackDeregister(
      PriceOracleContractState state, BlockchainAddress account) {
    return callbackDeregister(state, account, true);
  }

  private PriceOracleContractState callbackDeregister(
      PriceOracleContractState state, BlockchainAddress account, boolean success) {
    CallbackContext callbackContext = createCallbackContext(success);
    return serialization.callback(
        context, callbackContext, state, PriceOracleContract.Callbacks.deregister(account));
  }

  private PriceOracleContractState callbackRegister(
      PriceOracleContractState state, BlockchainAddress account) {
    return callbackRegister(state, account, true);
  }

  private PriceOracleContractState callbackRegister(
      PriceOracleContractState state, BlockchainAddress account, boolean success) {
    CallbackContext callbackContext = createCallbackContext(success);
    return serialization.callback(
        context, callbackContext, state, PriceOracleContract.Callbacks.register(account));
  }

  private PriceOracleContractState callbackStartDispute(
      PriceOracleContractState state,
      BlockchainAddress challenger,
      RoundData originalClaim,
      boolean success) {
    return callbackStartDispute(state, challenger, originalClaim, null, success);
  }

  private PriceOracleContractState callbackStartDispute(
      PriceOracleContractState state,
      BlockchainAddress challenger,
      RoundData originalClaim,
      RoundData newClaim,
      boolean success) {
    CallbackContext callbackContext = createCallbackContext(success);
    return serialization.callback(
        context,
        callbackContext,
        state,
        PriceOracleContract.Callbacks.startDispute(challenger, originalClaim, newClaim));
  }

  private PriceOracleContractState invokeDeregister(PriceOracleContractState state) {
    return serialization.invoke(
        context, state, s -> s.writeByte(PriceOracleContract.Invocations.DEREGISTER));
  }

  private PriceOracleContractState invokeDisputeCounterClaim(
      PriceOracleContractState state, RoundDataRpc counterClaim) {
    return serialization.invoke(
        context,
        state,
        s -> {
          s.writeByte(PriceOracleContract.Invocations.DISPUTE_COUNTER_CLAIM);
          writeRoundDataRpc(counterClaim, s);
        });
  }

  private PriceOracleContractState invokeNotifyPriceUpdate(
      RoundDataRpc roundData, PriceOracleContractState state) {
    return serialization.invoke(
        context,
        state,
        s -> {
          s.writeByte(PriceOracleContract.Invocations.NOTIFY_PRICE_UPDATE);
          writeRoundDataRpc(roundData, s);
        });
  }

  private void writeRoundDataRpc(RoundDataRpc roundData, SafeDataOutputStream stream) {
    stream.write(roundData.roundId());
    roundData.answer().write(stream);
    roundData.startedAt().write(stream);
    roundData.updatedAt().write(stream);
    roundData.answeredInRound().write(stream);
  }

  private PriceOracleContractState invokeRegister(PriceOracleContractState state) {
    return serialization.invoke(
        context, state, s -> s.writeByte(PriceOracleContract.Invocations.REGISTER));
  }

  private PriceOracleContractState invokeStartDispute(
      PriceOracleContractState state, Unsigned256 roundId) {
    return serialization.invoke(
        context,
        state,
        s -> {
          s.writeByte(PriceOracleContract.Invocations.START_DISPUTE);
          roundId.write(s);
        });
  }

  private PriceOracleContractState invokeDisputeResult(
      PriceOracleContractState state, long oracleId, long disputeId, int result) {
    return serialization.invoke(
        context,
        state,
        s -> {
          s.writeByte(PriceOracleContract.Invocations.DISPUTE_RESULT);
          s.writeLong(oracleId);
          s.writeLong(disputeId);
          s.writeInt(result);
        });
  }

  private PriceOracleContractState invokeOptIn(PriceOracleContractState state) {
    return serialization.invoke(
        context, state, s -> s.writeByte(PriceOracleContract.Invocations.OPT_IN));
  }

  private PriceOracleContractState invokeOptOut(PriceOracleContractState state) {
    return serialization.invoke(
        context, state, s -> s.writeByte(PriceOracleContract.Invocations.OPT_OUT));
  }

  private PriceOracleContractState registerAccounts(
      PriceOracleContractState state, List<BlockchainAddress> accounts) {
    for (BlockchainAddress account : accounts) {
      from(account);
      state = invokeRegister(state);
      state = callbackRegister(state, account);
      assertThat(state.isRegistered(account)).isTrue();
    }
    return state;
  }

  private PriceOracleContractState startChallengePeriod(
      PriceOracleContractState state,
      RoundDataRpc roundData,
      BlockchainAddress accountA,
      BlockchainAddress accountB,
      BlockchainAddress accountC) {
    from(accountA);
    PriceOracleContractState roundUpdateFromA = invokeNotifyPriceUpdate(roundData, state);
    from(accountB);
    PriceOracleContractState roundUpdateFromB =
        invokeNotifyPriceUpdate(roundData, roundUpdateFromA);
    from(accountC);
    PriceOracleContractState roundUpdateFromC =
        invokeNotifyPriceUpdate(roundData, roundUpdateFromB);
    assertThat(context.getUpdateGlobalAccountPluginState()).isNull();
    assertThat(roundUpdateFromC.hasChallengePeriod()).isTrue();
    return roundUpdateFromC;
  }

  private CallbackContext createCallbackContext(boolean success) {
    CallbackContext.ExecutionResult executionResult = createExecutionResult(success);
    return CallbackContext.create(FixedList.create(List.of(executionResult)));
  }

  private CallbackContext.ExecutionResult createExecutionResult(boolean success) {
    return CallbackContext.createResult(
        Hash.create(h -> h.writeString("EVENT_TRANSACTION")),
        success,
        SafeDataInputStream.createFromBytes(new byte[0]));
  }

  private PriceOracleContractState createInitial() {
    return PriceOracleContractState.initial(
        symbol, largeOracleContractAddress, null, decimals, chainId);
  }

  private void from(BlockchainAddress from) {
    context =
        new SysContractContextTest(context.getBlockProductionTime(), context.getBlockTime(), from);
  }

  private void increaseBlockProductionTime(long millis) {
    context =
        new SysContractContextTest(
            context.getBlockProductionTime() + millis, context.getBlockTime(), context.getFrom());
  }

  private void increaseBlockProductionTimeSeconds(long seconds) {
    long millis = seconds * 1000;
    increaseBlockProductionTime(millis);
  }

  private static RoundDataRpc testRound(
      long roundId, long answer, long startedAt, long updatedAt, long answeredInRound) {

    return new RoundDataRpc(
        Arrays.copyOfRange(Unsigned256.create(roundId).serialize(), 32 - 10, 32),
        Unsigned256.create(answer),
        Unsigned256.create(startedAt),
        Unsigned256.create(updatedAt),
        Unsigned256.create(answeredInRound));
  }
}
