package com.partisiablockchain.governance.priceoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import java.util.ArrayList;
import java.util.List;

/** Holds notified round data from oracle nodes for a specific round. */
@Immutable
final class PriceUpdates implements StateSerializable {

  /**
   * The number of agreeing price updates for the same round needed before starting a challenge
   * period.
   */
  static final int CHALLENGE_PERIOD_THRESHOLD = 3;

  private final AvlTree<BlockchainAddress, RoundData> oraclePriceUpdates;

  @SuppressWarnings("unused")
  private PriceUpdates() {
    this.oraclePriceUpdates = null;
  }

  private PriceUpdates(AvlTree<BlockchainAddress, RoundData> oraclePriceUpdates) {
    this.oraclePriceUpdates = oraclePriceUpdates;
  }

  static PriceUpdates initial(BlockchainAddress sender, RoundData roundPrice) {
    AvlTree<BlockchainAddress, RoundData> oraclePriceUpdates = AvlTree.create();
    return new PriceUpdates(oraclePriceUpdates.set(sender, roundPrice));
  }

  static PriceUpdates createFromStateAccessor(StateAccessor oldState) {
    AvlTree<BlockchainAddress, RoundData> oraclePriceUpdates =
        StateAccessorUtil.toAvlTree(
            oldState.get("oraclePriceUpdates"),
            RoundData::createFromStateAccessor,
            BlockchainAddress.class);
    return new PriceUpdates(oraclePriceUpdates);
  }

  PriceUpdates addPriceUpdate(BlockchainAddress sender, RoundData roundData) {
    return new PriceUpdates(oraclePriceUpdates.set(sender, roundData));
  }

  PriceUpdates removePriceUpdate(BlockchainAddress sender) {
    return new PriceUpdates(oraclePriceUpdates.remove(sender));
  }

  RoundData getPriceUpdate(BlockchainAddress address) {
    return oraclePriceUpdates.getValue(address);
  }

  /**
   * Determines the price from the received price updates.
   *
   * @return the price
   */
  Unsigned256 determinePrice() {
    return oraclePriceUpdates.values().get(0).answer();
  }

  /**
   * Retrieves initial price update.
   *
   * @return the first update
   */
  RoundData getOriginalUpdate() {
    return oraclePriceUpdates.values().get(0);
  }

  /**
   * Checks if enough price updates has been notified to start a challenge period.
   *
   * @return true if a challenge period should be started otherwise false.
   */
  boolean hasExceededChallengePeriodThreshold() {
    return oraclePriceUpdates.size() >= CHALLENGE_PERIOD_THRESHOLD;
  }

  List<BlockchainAddress> getNotifyingOracleNodes() {
    return new ArrayList<>(oraclePriceUpdates.keySet());
  }

  /**
   * Checks if any of the notified price updates are disputing. A price update is disputing if it is
   * not in agreement with existing price updates or if the special "illegal round id" round data
   * has been notified.
   *
   * @return true if disputing prices has been notified for this round otherwise false.
   */
  boolean containsDisputingPrices() {
    boolean shouldStartDispute = false;
    Unsigned256 answer = determinePrice();
    for (RoundData roundData : oraclePriceUpdates.values()) {
      if (!roundData.answer().equals(answer)) {
        shouldStartDispute = true;
      } else if (roundData.isIllegalRoundIdRoundData()) {
        shouldStartDispute = true;
      }
    }
    return shouldStartDispute;
  }
}
