package com.partisiablockchain.governance.priceoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.math.Unsigned256;
import java.nio.ByteBuffer;

/**
 * Reflects the representation of a dispute id on the large oracle contract.
 *
 * @param oracleId the oracle id
 * @param disputeId the dispute id
 */
record LargeOracleDisputeId(long oracleId, long disputeId) {

  /**
   * Split the round id into two longs to fit into the existing dispute logic on the large oracle
   * contract.
   *
   * <p>To use a price oracle round id as the dispute id on the large oracle contract the round id
   * has been split into two longs. This is possible since a round id is an uint80 stored as an
   * Unsigned256.
   *
   * @param roundId the price oracle round id
   * @return large oracle dispute id
   */
  static LargeOracleDisputeId fromRoundId(Unsigned256 roundId) {
    byte[] unsigned128 = new byte[16];
    System.arraycopy(roundId.serialize(), 16, unsigned128, 0, 16);
    ByteBuffer wrapper = ByteBuffer.wrap(unsigned128);
    long oracleId = wrapper.getLong();
    long disputeId = wrapper.getLong();
    return new LargeOracleDisputeId(oracleId, disputeId);
  }

  /**
   * Converts from large oracle dispute id to a price oracle round id.
   *
   * @return the recreated round id
   */
  Unsigned256 toRoundId() {
    return Unsigned256.create(
        ByteBuffer.allocate(Long.BYTES * 2).putLong(oracleId).putLong(disputeId).array());
  }
}
