package com.partisiablockchain.governance.priceoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;

/**
 * Holds information about all rounds that nodes have notified round data for since the latest
 * published price.
 */
@Immutable
final class RoundPriceUpdates implements StateSerializable {

  private final AvlTree<Unsigned256, PriceUpdates> roundPriceUpdates;

  @SuppressWarnings("unused")
  private RoundPriceUpdates() {
    this.roundPriceUpdates = null;
  }

  private RoundPriceUpdates(AvlTree<Unsigned256, PriceUpdates> roundPriceUpdates) {
    this.roundPriceUpdates = roundPriceUpdates;
  }

  static RoundPriceUpdates create() {
    return new RoundPriceUpdates(AvlTree.create());
  }

  static RoundPriceUpdates createFromStateAccessor(StateAccessor stateAccessor) {
    AvlTree<Unsigned256, PriceUpdates> roundPriceUpdates =
        StateAccessorUtil.toAvlTree(
            stateAccessor.get("roundPriceUpdates"),
            PriceUpdates::createFromStateAccessor,
            Unsigned256.class);
    return new RoundPriceUpdates(roundPriceUpdates);
  }

  PriceUpdates getPriceUpdatesForRound(Unsigned256 roundId) {
    return roundPriceUpdates.getValue(roundId);
  }

  RoundPriceUpdates removeOldRounds(Unsigned256 latestRoundId) {
    AvlTree<Unsigned256, PriceUpdates> updatedRoundPriceUpdates = AvlTree.create();
    for (Unsigned256 roundId : roundPriceUpdates.keySet()) {
      if (roundId.compareTo(latestRoundId) > 0) {
        updatedRoundPriceUpdates =
            updatedRoundPriceUpdates.set(roundId, getPriceUpdatesForRound(roundId));
      }
    }
    return new RoundPriceUpdates(updatedRoundPriceUpdates);
  }

  RoundPriceUpdates removePriceUpdatesForRound(Unsigned256 roundId) {
    return new RoundPriceUpdates(roundPriceUpdates.remove(roundId));
  }

  RoundPriceUpdates removePriceUpdatesForNode(BlockchainAddress node) {
    AvlTree<Unsigned256, PriceUpdates> updatedRoundPriceUpdates = roundPriceUpdates;
    for (Unsigned256 roundId : updatedRoundPriceUpdates.keySet()) {
      PriceUpdates priceUpdatesForRound = updatedRoundPriceUpdates.getValue(roundId);
      if (priceUpdatesForRound.getPriceUpdate(node) != null) {
        PriceUpdates updatedPriceUpdates = priceUpdatesForRound.removePriceUpdate(node);
        updatedRoundPriceUpdates = updatedRoundPriceUpdates.set(roundId, updatedPriceUpdates);
      }
    }
    return new RoundPriceUpdates(updatedRoundPriceUpdates);
  }

  boolean hasPriceUpdateFromNode(BlockchainAddress nodeAddress) {
    for (PriceUpdates priceUpdate : roundPriceUpdates.values()) {
      if (priceUpdate.getNotifyingOracleNodes().contains(nodeAddress)) {
        return true;
      }
    }
    return false;
  }

  RoundPriceUpdates addPriceUpdate(RoundData roundData, BlockchainAddress sender) {
    Unsigned256 roundId = roundData.roundId();
    boolean existingRound = roundPriceUpdates.containsKey(roundId);
    PriceUpdates priceUpdates;
    if (existingRound) {
      priceUpdates = roundPriceUpdates.getValue(roundId);
      priceUpdates = priceUpdates.addPriceUpdate(sender, roundData);
    } else {
      priceUpdates = PriceUpdates.initial(sender, roundData);
    }
    AvlTree<Unsigned256, PriceUpdates> updatedRoundPriceUpdates =
        roundPriceUpdates.set(roundId, priceUpdates);
    return new RoundPriceUpdates(updatedRoundPriceUpdates);
  }
}
