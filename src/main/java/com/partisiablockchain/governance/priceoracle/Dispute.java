package com.partisiablockchain.governance.priceoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.tools.immutable.FixedList;
import java.util.List;

/**
 * Started when disagreeing prices have been notified for the same round or when a dispute is
 * started by manually invoking {@link PriceOracleContract.Invocation#START_DISPUTE}. Holds
 * information about the challenger that started the dispute and the claims.
 */
@Immutable
final class Dispute implements StateSerializable {

  private final FixedList<RoundData> claims;
  private final BlockchainAddress challenger;

  @SuppressWarnings("unused")
  private Dispute() {
    this.claims = null;
    this.challenger = null;
  }

  Dispute(FixedList<RoundData> claims, BlockchainAddress challenger) {
    this.claims = claims;
    this.challenger = challenger;
  }

  FixedList<RoundData> getClaims() {
    return claims;
  }

  BlockchainAddress getChallenger() {
    return challenger;
  }

  RoundData getOriginalClaim() {
    return claims.get(0);
  }

  static Dispute create(RoundData originalClaim, RoundData newClaim, BlockchainAddress challenger) {
    return new Dispute(FixedList.create(List.of(originalClaim, newClaim)), challenger);
  }

  static Dispute create(RoundData originalClaim, BlockchainAddress challenger) {
    return new Dispute(FixedList.create(List.of(originalClaim)), challenger);
  }

  static Dispute fromStateAccessor(StateAccessor accessor) {
    if (accessor.isNull()) {
      return null;
    } else {
      List<RoundData> claims =
          accessor.get("claims").getListElements().stream()
              .map(RoundData::createFromStateAccessor)
              .toList();
      BlockchainAddress challenger = accessor.get("challenger").blockchainAddressValue();
      return new Dispute(FixedList.create(claims), challenger);
    }
  }

  Dispute addCounterClaim(RoundData counterClaim) {
    if (claims.contains(counterClaim)) {
      return this;
    } else {
      return new Dispute(claims.addElement(counterClaim), challenger);
    }
  }
}
