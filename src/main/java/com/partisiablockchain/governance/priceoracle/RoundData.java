package com.partisiablockchain.governance.priceoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.stream.SafeDataOutputStream;

/** Price information for a round. */
@Immutable
public record RoundData(
    Unsigned256 roundId,
    Unsigned256 answer,
    Unsigned256 startedAt,
    Unsigned256 updatedAt,
    Unsigned256 answeredInRound)
    implements StateSerializable {

  void write(SafeDataOutputStream stream) {
    roundId.write(stream);
    answer.write(stream);
    startedAt.write(stream);
    updatedAt.write(stream);
    answeredInRound.write(stream);
  }

  /**
   * Checks if the round data is historic or from the future.
   *
   * @param blockProductionTime the time to compare to in milliseconds
   * @return true if the round data is historic otherwise false
   */
  boolean isHistoric(long blockProductionTime) {
    // Convert block production time to seconds instead of milliseconds to match format of round
    // data from Chainlink.
    Unsigned256 blockProductionTimeInSeconds =
        Unsigned256.create(Math.floorDiv(blockProductionTime, 1000));
    Unsigned256 updatedAt = updatedAt();
    return updatedAt.compareTo(blockProductionTimeInSeconds) <= 0;
  }

  /**
   * Checks if this round data is the special "illegal round id round data" consisting of a round id
   * and only zeros, which indicates that the round id is illegal and valid round data can not be
   * retrieved.
   *
   * @return true if this round data indicates an illegal round id otherwise false
   */
  boolean isIllegalRoundIdRoundData() {
    return answer.equals(Unsigned256.ZERO)
        && startedAt.equals(Unsigned256.ZERO)
        && updatedAt.equals(Unsigned256.ZERO)
        && answeredInRound.equals(Unsigned256.ZERO);
  }

  static RoundData createFromStateAccessor(StateAccessor stateAccessor) {
    Unsigned256 roundId = stateAccessor.get("roundId").cast(Unsigned256.class);
    Unsigned256 answer = stateAccessor.get("answer").cast(Unsigned256.class);
    Unsigned256 startedAt = stateAccessor.get("startedAt").cast(Unsigned256.class);
    Unsigned256 updatedAt = stateAccessor.get("updatedAt").cast(Unsigned256.class);
    Unsigned256 answeredInRound = stateAccessor.get("answeredInRound").cast(Unsigned256.class);
    return new RoundData(roundId, answer, startedAt, updatedAt, answeredInRound);
  }
}
