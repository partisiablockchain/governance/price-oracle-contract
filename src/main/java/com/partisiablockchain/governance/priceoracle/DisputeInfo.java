package com.partisiablockchain.governance.priceoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;

/**
 * Holds information about the dispute, whether it is pending or not, and if a dispute has been
 * queued in the meantime. A dispute is pending while waiting for the callback of the invocation to
 * the large oracle contract. If a dispute is started while a dispute is currently pending the new
 * dispute is instead queued.
 */
@Immutable
record DisputeInfo(Dispute dispute, boolean pending, Dispute queued) implements StateSerializable {

  static DisputeInfo create() {
    return new DisputeInfo(null, false, null);
  }

  static DisputeInfo createFromStateAccessor(StateAccessor stateAccessor) {
    return new DisputeInfo(
        Dispute.fromStateAccessor(stateAccessor.get("dispute")),
        stateAccessor.get("pending").booleanValue(),
        Dispute.fromStateAccessor(stateAccessor.get("queued")));
  }

  DisputeInfo markAsPending() {
    return new DisputeInfo(dispute, true, queued);
  }

  DisputeInfo setQueuedDispute(Dispute queuedDispute) {
    return new DisputeInfo(dispute, pending, queuedDispute);
  }

  DisputeInfo beginDispute(Dispute dispute) {
    return new DisputeInfo(dispute, false, null);
  }

  DisputeInfo addCounterClaim(RoundData counterClaim) {
    Dispute updatedDispute = dispute.addCounterClaim(counterClaim);
    return new DisputeInfo(updatedDispute, pending, queued);
  }

  DisputeInfo clear() {
    return new DisputeInfo(null, false, null);
  }
}
