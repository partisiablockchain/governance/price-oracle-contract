package com.partisiablockchain.governance.priceoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;

/**
 * Holds information about the number of decimals used to denote the BYOC on PBC and the USD price.
 *
 * @param pbc number of decimals used to denote the BYOC balance on PBC
 * @param usd number of decimals used to denote the price in USD on Chainlink
 */
@Immutable
public record Decimals(int pbc, int usd) implements StateSerializable {

  static Decimals createFromStateAccessor(StateAccessor accessor) {
    return new Decimals(accessor.get("pbc").intValue(), accessor.get("usd").intValue());
  }
}
