package com.partisiablockchain.governance.priceoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.FormatMethod;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.reflect.Action;
import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Callback;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.contract.reflect.RpcType;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.contract.sys.SystemEventCreator;
import com.partisiablockchain.contract.sys.SystemEventManager;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.secata.stream.DataStreamSerializable;
import java.util.ArrayList;
import java.util.List;

/** A system contract for facilitating price oracle services. */
@AutoSysContract(PriceOracleContractState.class)
public final class PriceOracleContract {

  static final int NO_FRAUD = -1;

  /** Invocation shortnames. */
  static final class Invocations {
    static final int REGISTER = 0;
    static final int NOTIFY_PRICE_UPDATE = 1;
    static final int DISPUTE_RESULT = 2;
    static final int DEREGISTER = 4;
    static final int DISPUTE_COUNTER_CLAIM = 5;
    static final int START_DISPUTE = 6;
    static final int OPT_OUT = 7;
    static final int OPT_IN = 8;

    private Invocations() {}
  }

  /** Callback shortnames. */
  static final class Callbacks {
    static final int CALLBACK_REGISTER = 1;
    static final int CALLBACK_DEREGISTER = 2;
    static final int CALLBACK_START_DISPUTE = 3;

    private Callbacks() {}

    static DataStreamSerializable register(BlockchainAddress sender) {
      return s -> {
        s.writeByte(PriceOracleContract.Callbacks.CALLBACK_REGISTER);
        sender.write(s);
      };
    }

    static DataStreamSerializable deregister(BlockchainAddress sender) {
      return s -> {
        s.writeByte(PriceOracleContract.Callbacks.CALLBACK_DEREGISTER);
        sender.write(s);
      };
    }

    static DataStreamSerializable startDispute(
        BlockchainAddress challenger, RoundData originalClaim) {
      return startDispute(challenger, originalClaim, null);
    }

    static DataStreamSerializable startDispute(
        BlockchainAddress challenger, RoundData originalClaim, RoundData newClaim) {
      return s -> {
        s.writeByte(PriceOracleContract.Callbacks.CALLBACK_START_DISPUTE);
        challenger.write(s);
        originalClaim.write(s);
        s.writeOptional(RoundData::write, newClaim);
      };
    }
  }

  /**
   * Initialize the price oracle contract.
   *
   * @param symbol coin symbol the price is getting updated for
   * @param largeOracleContractAddress address of the large oracle contract
   * @param referenceContractAddress Chainlink contract address the prices are read from
   * @param decimals number of decimals used to denote the BYOC
   * @param chainId the EVM chain of the reference contract.
   * @return initial state
   */
  @Init
  public PriceOracleContractState create(
      String symbol,
      BlockchainAddress largeOracleContractAddress,
      String referenceContractAddress,
      Decimals decimals,
      String chainId) {
    return PriceOracleContractState.initial(
        symbol, largeOracleContractAddress, referenceContractAddress, decimals, chainId);
  }

  /**
   * Migrate an existing contract to this version.
   *
   * @param oldState accessor for the old state
   * @return the migrated state
   */
  @Upgrade
  public PriceOracleContractState upgrade(StateAccessor oldState) {
    return PriceOracleContractState.migrateState(oldState);
  }

  /**
   * Register new price oracle node. Only accounts with enough staked tokens can register as a node.
   * This invocation registers a callback - see {@link Callbacks#CALLBACK_REGISTER}.
   *
   * @param context execution context
   * @param state current state
   * @return updated state with the sender being marked as pending
   */
  @Action(Invocations.REGISTER)
  public PriceOracleContractState register(
      SysContractContext context, PriceOracleContractState state) {
    BlockchainAddress sender = context.getFrom();
    ensure(
        !state.isNodePending(sender),
        "Cannot register while waiting on response from another register or deregister");
    ensure(
        !state.isRegistered(sender) && !state.isNodeOptedOut(sender),
        "Cannot register an account that has already been registered");

    SystemEventManager eventManager = context.getRemoteCallsCreator();
    eventManager
        .invoke(state.getLargeOracleContractAddress())
        .withPayload(LargeOracleRpc.lockTokensToPriceOracle(sender))
        .sendFromContract();
    eventManager.registerCallbackWithCostFromRemaining(Callbacks.register(sender));
    return state.setNodeToPending(sender);
  }

  /**
   * Deregister a registered price oracle node. To be allowed to deregister the node must not have
   * notified any price updates for ongoing rounds. This invocation registers a callback - see
   * {@link Callbacks#CALLBACK_DEREGISTER}.
   *
   * @param context execution context
   * @param state current state
   * @return updated state with the sender being marked as pending
   */
  @Action(Invocations.DEREGISTER)
  public PriceOracleContractState deregister(
      SysContractContext context, PriceOracleContractState state) {
    BlockchainAddress sender = context.getFrom();
    ensure(
        state.isRegistered(sender) || state.isNodeOptedOut(sender),
        "Only registered oracle nodes can be deregistered");
    ensure(
        !state.hasPriceUpdateFromNode(sender),
        "Cannot deregister an oracle node that has notified a price update on an ongoing"
            + " round");

    SystemEventManager eventManager = context.getRemoteCallsCreator();
    eventManager
        .invoke(state.getLargeOracleContractAddress())
        .withPayload(LargeOracleRpc.unlockTokensFromPriceOracle(sender))
        .sendFromContract();
    eventManager.registerCallbackWithCostFromRemaining(Callbacks.deregister(sender));
    return state.setNodeToPending(sender);
  }

  /**
   * Notifies a price update for a specific round.
   *
   * <p>If {@link PriceUpdates#CHALLENGE_PERIOD_THRESHOLD} agreeing price updates have been notified
   * for the same round a challenge period is started.
   *
   * <p>If the notified round data does not match existing round data for the round a dispute will
   * be started. This will register a callback - see {@link Callbacks#CALLBACK_START_DISPUTE}.
   *
   * <p>If a challenge period has ended this invocation will also result in the new price being
   * published.
   *
   * @param context execution context
   * @param state current state
   * @param roundData round data object representing the notified price
   * @return updated state
   */
  @Action(Invocations.NOTIFY_PRICE_UPDATE)
  public PriceOracleContractState notifyPriceUpdate(
      SysContractContext context, PriceOracleContractState state, RoundDataRpc roundData) {
    final BlockchainAddress sender = context.getFrom();
    final long blockProductionTime = context.getBlockProductionTime();
    RoundData roundDataStored = roundData.convert();
    final Unsigned256 roundId = roundDataStored.roundId();

    ensure(
        !state.isNodeOptedOut(sender),
        "Price updates can not be notified by a node that has opted out of notifying");
    ensure(state.isRegistered(sender), "Only oracle members can notify price updates");
    ensure(!state.hasDispute(), "Price updates cannot be notified while a dispute is ongoing");
    ensure(
        !state.hasChallengePeriod()
            || state.getChallengePeriod().roundId().equals(roundId)
            || state.isChallengePeriodOver(blockProductionTime),
        "Only price updates for the round of the ongoing challenge period are allowed");
    ensure(
        !roundDataStored.isIllegalRoundIdRoundData() || state.hasChallengePeriod(),
        "It is only allowed to notify the special \"illegal round id\" round data during a"
            + " challenge period");
    ensure(
        roundDataStored.isHistoric(blockProductionTime),
        "Round data is after block production time");
    boolean newerRound = roundId.compareTo(state.getLatestPublishedRoundId()) > 0;
    ensure(
        newerRound,
        "Only price updates for rounds newer than the latest published round are allowed");

    PriceOracleContractState updatedState;
    boolean shouldPublishPrice =
        state.isChallengePeriodOver(blockProductionTime) && !state.hasPendingDispute();
    if (shouldPublishPrice) {
      updatedState = publishPrice(context, state);

      if (!state.getChallengePeriod().roundId().equals(roundId)) {
        updatedState = addPriceUpdate(sender, updatedState, roundDataStored);
      }
    } else {
      updatedState = addPriceUpdate(sender, state, roundDataStored);
    }

    PriceUpdates updatedPriceUpdates = updatedState.getPriceUpdatesForRound(roundId);
    if (updatedPriceUpdates != null) {
      if (shouldStartDispute(updatedPriceUpdates)) {
        RoundData originalClaim = state.getPriceUpdatesForRound(roundId).getOriginalUpdate();
        Dispute dispute = Dispute.create(originalClaim, roundDataStored, sender);
        if (state.hasPendingDispute()) {
          updatedState = updatedState.setQueuedDispute(dispute);
        } else {
          initiateDispute(
              sender,
              originalClaim,
              roundDataStored,
              0,
              state.getLargeOracleContractAddress(),
              context.getRemoteCallsCreator());
          updatedState = updatedState.markDisputeAsPending();
        }
      }
      if (shouldStartChallengePeriod(updatedPriceUpdates, updatedState.hasChallengePeriod())) {
        return updatedState.startChallengePeriod(blockProductionTime, roundId);
      }
    }
    return updatedState;
  }

  private PriceOracleContractState publishPrice(
      SysContractContext context, PriceOracleContractState state) {
    // The round data to publish has to be retrieved from the round id of the challenge
    // period, since the just notified round in this case might be for a different round id
    Unsigned256 challengePeriodRoundId = state.getChallengePeriod().roundId();
    PriceUpdates publishPriceUpdates = state.getPriceUpdatesForRound(challengePeriodRoundId);
    PricePublisher.publish(
        state.getSymbol(),
        publishPriceUpdates.determinePrice(),
        publishPriceUpdates.getNotifyingOracleNodes(),
        context,
        state.getDecimals());
    PriceOracleContractState updatedState = state.setLatestPublishedRoundId(challengePeriodRoundId);
    updatedState = updatedState.removeOldRounds(challengePeriodRoundId);
    return updatedState.resetChallengePeriod();
  }

  private PriceOracleContractState addPriceUpdate(
      BlockchainAddress sender, PriceOracleContractState state, RoundData roundData) {
    Unsigned256 roundId = roundData.roundId();
    PriceUpdates priceUpdates = state.getPriceUpdatesForRound(roundId);
    ensure(
        priceUpdates == null || priceUpdates.getPriceUpdate(sender) == null,
        "Price updates can not be notified more than once");
    return state.addPriceUpdate(roundData, sender);
  }

  private boolean shouldStartDispute(PriceUpdates priceUpdates) {
    return priceUpdates.containsDisputingPrices();
  }

  private boolean shouldStartChallengePeriod(
      PriceUpdates priceUpdates, boolean challengePeriodActive) {
    return priceUpdates.hasExceededChallengePeriodThreshold() && !challengePeriodActive;
  }

  private static void initiateDispute(
      BlockchainAddress challenger,
      RoundData originalClaim,
      RoundData newClaim,
      long cost,
      BlockchainAddress largeOracleContractAddress,
      SystemEventManager eventManager) {
    DataStreamSerializable rpc =
        LargeOracleRpc.createDisputePoll(challenger, originalClaim.roundId(), cost);
    eventManager.invoke(largeOracleContractAddress).withPayload(rpc).sendFromContract();
    eventManager.registerCallbackWithCostFromRemaining(
        Callbacks.startDispute(challenger, originalClaim, newClaim));
  }

  /**
   * Allows a non-registered account to start a dispute for an ongoing challenge period. The account
   * needs to have staked {@link LargeOracleRpc#REGISTER_STAKED_TOKENS} towards the large oracle
   * contract. This invocation registers a callback - see {@link Callbacks#CALLBACK_START_DISPUTE}.
   *
   * @param context execution context
   * @param state current state
   * @param roundId id for the round that a dispute should be started for
   * @return updated state with a dispute started for the given round id
   */
  @Action(Invocations.START_DISPUTE)
  public PriceOracleContractState startDispute(
      SysContractContext context, PriceOracleContractState state, Unsigned256 roundId) {
    final BlockchainAddress sender = context.getFrom();

    ensure(state.getDispute() == null, "There is already an ongoing dispute");
    ensure(!state.hasPendingDispute(), "There is already a dispute pending for this round");
    ensure(
        state.hasChallengePeriod() && state.getChallengePeriod().roundId().equals(roundId),
        "There is not an active challenge period for this round id");
    ensure(
        !state.isChallengePeriodOver(context.getBlockProductionTime()),
        "A dispute can not be started for a challenge period that is over");

    PriceUpdates updatedPriceUpdates = state.getPriceUpdatesForRound(roundId);
    RoundData originalClaim = updatedPriceUpdates.getOriginalUpdate();

    initiateDispute(
        sender,
        originalClaim,
        null,
        LargeOracleRpc.REGISTER_STAKED_TOKENS,
        state.getLargeOracleContractAddress(),
        context.getRemoteCallsCreator());

    return state.markDisputeAsPending();
  }

  /**
   * Adds a counter-claim for an ongoing dispute. Can only be added through the large oracle
   * contract by the committee.
   *
   * @param context execution context
   * @param state current state
   * @param roundData round data that represents the counter claim
   * @return updated state with the given counter claim added
   */
  @Action(Invocations.DISPUTE_COUNTER_CLAIM)
  public PriceOracleContractState disputeCounterClaim(
      SysContractContext context, PriceOracleContractState state, RoundDataRpc roundData) {
    BlockchainAddress largeOracle = state.getLargeOracleContractAddress();
    BlockchainAddress sender = context.getFrom();
    RoundData roundDataStored = roundData.convert();
    ensure(
        largeOracle.equals(sender),
        "Counter-claims can only be added through the large oracle contract");
    ensure(state.getDispute() != null, "No dispute to add counter-claim to");
    return state.addCounterClaim(roundDataStored);
  }

  /**
   * Submits the result of the ongoing dispute and burns fraudulent nodes. Can only be added through
   * the large oracle contract after the committee has finished voting.
   *
   * @param context execution context
   * @param state current state
   * @param oracleId id of the oracle that this dispute relates to
   * @param disputeId id for the dispute
   * @param disputeResult the result of the vote
   * @return updated state with the fraudulent nodes burned and the challenge period ended
   */
  @Action(Invocations.DISPUTE_RESULT)
  public PriceOracleContractState disputeResult(
      SysContractContext context,
      PriceOracleContractState state,
      long oracleId,
      long disputeId,
      int disputeResult) {

    ensure(
        state.getLargeOracleContractAddress().equals(context.getFrom()),
        "Dispute can only be resolved by large oracle contract");
    ensure(state.getDispute() != null, "Dispute does not exist");
    Dispute dispute = state.getDispute();

    Unsigned256 roundId = new LargeOracleDisputeId(oracleId, disputeId).toRoundId();
    Unsigned256 disputeRoundId = dispute.getOriginalClaim().roundId();
    ensure(disputeRoundId.equals(roundId), "Dispute result was for another dispute");

    RoundData resultClaim = dispute.getClaims().get(disputeResult == NO_FRAUD ? 0 : disputeResult);

    List<BlockchainAddress> maliciousNodes;
    PriceUpdates priceUpdates = state.getPriceUpdatesForRound(disputeRoundId);
    PriceOracleContractState updatedState = state;
    if (resultClaim.isIllegalRoundIdRoundData()) {
      // latest published round id stays the same and old rounds are kept
      maliciousNodes = priceUpdates.getNotifyingOracleNodes();
      updatedState = updatedState.removePriceUpdatesForRound(resultClaim.roundId());
    } else {
      maliciousNodes = determineMaliciousNodes(priceUpdates, resultClaim);
      List<BlockchainAddress> honestNodes =
          priceUpdates.getNotifyingOracleNodes().stream()
              .filter(n -> !maliciousNodes.contains(n))
              .toList();
      PricePublisher.publish(
          state.getSymbol(), resultClaim.answer(), honestNodes, context, state.getDecimals());
      Unsigned256 latestRoundId = resultClaim.roundId();
      updatedState = updatedState.setLatestPublishedRoundId(latestRoundId);
      updatedState = updatedState.removeOldRounds(latestRoundId);
    }
    burnStakedTokens(context, maliciousNodes, state.getLargeOracleContractAddress());

    for (BlockchainAddress maliciousNode : maliciousNodes) {
      updatedState = updatedState.removeNode(maliciousNode);
      updatedState = updatedState.removePriceUpdatesForNode(maliciousNode);
    }
    updatedState = updatedState.resetChallengePeriod();
    updatedState = updatedState.clearDispute();
    return updatedState;
  }

  private static List<BlockchainAddress> determineMaliciousNodes(
      PriceUpdates priceUpdates, RoundData resultClaim) {
    List<BlockchainAddress> maliciousNodes = new ArrayList<>();
    // All nodes that notified something other than the result is malicious
    for (BlockchainAddress notifyingOracleNode : priceUpdates.getNotifyingOracleNodes()) {
      RoundData notifiedRoundData = priceUpdates.getPriceUpdate(notifyingOracleNode);
      if (!notifiedRoundData.equals(resultClaim)) {
        maliciousNodes.add(notifyingOracleNode);
      }
    }
    return maliciousNodes;
  }

  private static void burnStakedTokens(
      SysContractContext context,
      List<BlockchainAddress> maliciousNodes,
      BlockchainAddress largeOracleContractAddress) {
    SystemEventCreator eventCreator = context.getInvocationCreator();
    eventCreator
        .invoke(largeOracleContractAddress)
        .withPayload(LargeOracleRpc.burnStakedTokens(maliciousNodes))
        .sendFromContract();
  }

  /**
   * Sets the contract to ignore any future price updates from the sending node, marking it as
   * OPTED_OUT. See also {@link #optIn OPT_IN}
   *
   * @param context the execution context
   * @param state the current contract state
   * @return updated contract state with the sending node set to be ignored
   */
  @Action(Invocations.OPT_OUT)
  public PriceOracleContractState optOut(
      SysContractContext context, PriceOracleContractState state) {
    ensure(state.isRegistered(context.getFrom()), "Only registered nodes can opt out");
    return state.setNodeToOptedOut(context.getFrom());
  }

  /**
   * Sets the contract to handle future price updates from the sending node again. See also {@link
   * #optOut OPT_OUT}
   *
   * @param context the execution context
   * @param state the current contract state
   * @return updated contract state with the sending node marked as REGISTERED
   */
  @Action(Invocations.OPT_IN)
  public PriceOracleContractState optIn(
      SysContractContext context, PriceOracleContractState state) {
    ensure(state.isNodeOptedOut(context.getFrom()), "Only nodes that have opted out can opt in");
    return state.registerNode(context.getFrom());
  }

  /**
   * Callback for registering to the price oracle contract.
   *
   * @param context execution context
   * @param state current state
   * @param callbackContext information about callbacks
   * @param nodeAddress address of the node that is registering
   * @return updated state
   */
  @Callback(Callbacks.CALLBACK_REGISTER)
  public PriceOracleContractState callbackRegister(
      SysContractContext context,
      PriceOracleContractState state,
      CallbackContext callbackContext,
      BlockchainAddress nodeAddress) {
    if (callbackContext.isSuccess()) {
      return state.registerNode(nodeAddress);
    } else {
      return state.removeNode(nodeAddress);
    }
  }

  /**
   * Callback for deregistering from the price oracle contract.
   *
   * @param context execution context
   * @param state current state
   * @param callbackContext information about callbacks
   * @param nodeAddress address of the node that is deregistering
   * @return updated state
   */
  @Callback(Callbacks.CALLBACK_DEREGISTER)
  public PriceOracleContractState callbackDeregister(
      SysContractContext context,
      PriceOracleContractState state,
      CallbackContext callbackContext,
      BlockchainAddress nodeAddress) {
    if (callbackContext.isSuccess()) {
      return state.removeNode(nodeAddress);
    } else {
      return state.registerNode(nodeAddress);
    }
  }

  /**
   * Callback for starting a dispute.
   *
   * @param context execution context
   * @param state current state
   * @param callbackContext information about callbacks
   * @param challenger node starting the dispute
   * @param originalClaim original claim for the dispute
   * @param newClaim new claim challenging the original claim
   * @return updated state
   */
  @Callback(Callbacks.CALLBACK_START_DISPUTE)
  public PriceOracleContractState callbackStartDispute(
      SysContractContext context,
      PriceOracleContractState state,
      CallbackContext callbackContext,
      BlockchainAddress challenger,
      RoundData originalClaim,
      @RpcType(nullable = true) RoundData newClaim) {
    if (callbackContext.isSuccess()) {
      if (newClaim == null) {
        return state.beginDispute(Dispute.create(originalClaim, challenger));
      } else {
        return state.beginDispute(Dispute.create(originalClaim, newClaim, challenger));
      }
    } else {
      Dispute queuedDispute = state.getQueuedDispute();
      if (queuedDispute == null) {
        return state.clearDispute();
      } else {
        // If the callback failed and another dispute was queued while waiting for the callback,
        // the queued dispute is started.
        SystemEventCreator eventCreator = context.getInvocationCreator();
        eventCreator
            .invoke(state.getLargeOracleContractAddress())
            .withPayload(
                LargeOracleRpc.createDisputePoll(
                    queuedDispute.getChallenger(), queuedDispute.getOriginalClaim().roundId()))
            .sendFromContract();
        return state.beginDispute(queuedDispute);
      }
    }
  }

  @FormatMethod
  private static void ensure(boolean predicate, String messageFormat, Object... args) {
    if (!predicate) {
      String errorMessage = String.format(messageFormat, args);
      throw new RuntimeException(errorMessage);
    }
  }
}
