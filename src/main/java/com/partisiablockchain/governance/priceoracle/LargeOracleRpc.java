package com.partisiablockchain.governance.priceoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.math.Unsigned256;
import com.secata.stream.DataStreamSerializable;
import java.util.List;

/** Helper class for generating RPC used for invocations to the large oracle contract. */
final class LargeOracleRpc {

  static final long REGISTER_STAKED_TOKENS = 5_000_0000L;

  static final byte BURN_STAKED_TOKENS = 3;
  static final byte LOCK_TOKENS_TO_PRICE_ORACLE = 12;
  static final byte UNLOCK_TOKENS_FROM_PRICE_ORACLE = 13;
  static final byte CREATE_DISPUTE_POLL = 14;

  @SuppressWarnings("unused")
  private LargeOracleRpc() {}

  static DataStreamSerializable lockTokensToPriceOracle(BlockchainAddress sender) {
    return s -> {
      s.writeByte(LOCK_TOKENS_TO_PRICE_ORACLE);
      s.writeLong(REGISTER_STAKED_TOKENS);
      sender.write(s);
    };
  }

  static DataStreamSerializable unlockTokensFromPriceOracle(BlockchainAddress sender) {
    return s -> {
      s.writeByte(UNLOCK_TOKENS_FROM_PRICE_ORACLE);
      s.writeLong(REGISTER_STAKED_TOKENS);
      sender.write(s);
    };
  }

  static DataStreamSerializable burnStakedTokens(List<BlockchainAddress> maliciousNodes) {
    return s -> {
      s.writeByte(BURN_STAKED_TOKENS);
      BlockchainAddress.LIST_SERIALIZER.writeDynamic(s, maliciousNodes);
      s.writeLong(REGISTER_STAKED_TOKENS);
    };
  }

  static DataStreamSerializable createDisputePoll(
      BlockchainAddress challenger, Unsigned256 roundId) {
    return createDisputePoll(challenger, roundId, 0);
  }

  static DataStreamSerializable createDisputePoll(
      BlockchainAddress challenger, Unsigned256 roundId, long cost) {

    LargeOracleDisputeId largeOracleRoundId = LargeOracleDisputeId.fromRoundId(roundId);
    return stream -> {
      stream.writeByte(CREATE_DISPUTE_POLL);
      challenger.write(stream);
      stream.writeLong(cost);
      stream.writeLong(largeOracleRoundId.oracleId());
      stream.writeLong(largeOracleRoundId.disputeId());
      stream.writeByte(PriceOracleContract.Invocations.DISPUTE_RESULT);
      stream.writeByte(PriceOracleContract.Invocations.DISPUTE_COUNTER_CLAIM);
    };
  }
}
