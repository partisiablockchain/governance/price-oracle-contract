package com.partisiablockchain.governance.priceoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import java.time.Duration;
import java.time.temporal.ChronoUnit;

/**
 * A period started immediately after a price has been agreed upon for a round, that allows the
 * price to be challenged. Holds the round id of the round it was started for and the block
 * production time for when it was started at.
 */
@Immutable
record ChallengePeriod(Unsigned256 roundId, long startedAt) implements StateSerializable {

  static final long CHALLENGE_PERIOD_DURATION_MILLIS = Duration.of(1, ChronoUnit.HOURS).toMillis();

  static ChallengePeriod createFromStateAccessor(StateAccessor stateAccessor) {
    if (stateAccessor.isNull()) {
      return null;
    } else {
      return new ChallengePeriod(
          stateAccessor.get("roundId").cast(Unsigned256.class),
          stateAccessor.get("startedAt").longValue());
    }
  }

  boolean hasEnded(long blockProductionTime) {
    return blockProductionTime > startedAt + CHALLENGE_PERIOD_DURATION_MILLIS;
  }

  static ChallengePeriod create(Unsigned256 activeChallengePeriod, long startedAt) {
    return new ChallengePeriod(activeChallengePeriod, startedAt);
  }
}
