package com.partisiablockchain.governance.priceoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.sys.GlobalPluginStateUpdate;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.math.Unsigned256;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.math.BigInteger;
import java.util.Collections;
import java.util.List;

/** Helper class for converting and publishing the new price to the account plugin. */
final class PricePublisher {

  /** AccountStateGlobal.GlobalInteraction#SET_COIN. */
  static final byte SET_COIN = 2;

  static final long GAS_REWARD_PER_PRICE_UPDATE = 50_000L;

  private PricePublisher() {}

  /**
   * Publishes the price.
   *
   * @param symbol the coin symbol
   * @param price the price to publish
   * @param notifyingOracleNodes the oracles that notified a price
   * @param decimals number of decimals used to denote the BYOC
   * @param context system contract context
   */
  static void publish(
      String symbol,
      Unsigned256 price,
      List<BlockchainAddress> notifyingOracleNodes,
      SysContractContext context,
      Decimals decimals) {
    ConversionRate conversionRate = convert(price, decimals);
    context
        .getInvocationCreator()
        .updateGlobalAccountPluginState(
            GlobalPluginStateUpdate.create(setCoin(symbol, conversionRate)));

    FixedList<BlockchainAddress> notifyingOracles = FixedList.create(notifyingOracleNodes);
    FixedList<Integer> weights = FixedList.create(Collections.nCopies(notifyingOracles.size(), 1));
    context.payInfrastructureFees(GAS_REWARD_PER_PRICE_UPDATE, notifyingOracles, weights);
  }

  static byte[] setCoin(String symbol, PricePublisher.ConversionRate conversionRate) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(SET_COIN);
          stream.writeString(symbol);
          stream.writeLong(conversionRate.numerator());
          stream.writeLong(conversionRate.denominator());
        });
  }

  /**
   * Find the conversion rate from BYOC to gas such that 1 USD worth of the coin gives 100.000 gas.
   *
   * <p>The calculation has to take into account that both the USD price and the balance in BYOC is
   * denoted by a different number of decimals depending on the BYOC. Additionally, the numerator
   * and denominator are truncated to fit into longs.
   *
   * <p>The conversion rate is calculated as:<br>
   *
   * <pre>
   *   c = (100.000 gas × <var>usdPrice</var>)
   *    / (10<sup><var>pbcDecimals</var></sup> × 10<sup><var>usdDecimals</var></sup>)
   * </pre>
   *
   * @param usdPrice the BYOC price in USD
   * @param decimals number of decimals used to denote the BYOC on PBC and USD price
   * @return the conversion rate
   */
  static ConversionRate convert(Unsigned256 usdPrice, Decimals decimals) {
    Unsigned256 gasPerUsd = Unsigned256.create(100_000);
    Unsigned256 numerator = gasPerUsd.multiply(usdPrice);
    Unsigned256 denominator =
        Unsigned256.create(BigInteger.TEN.pow(decimals.pbc() + decimals.usd()));
    return truncate(numerator, denominator);
  }

  /**
   * Truncate numerator and denominator if necessary to fit into longs at the expense of some
   * precision.
   *
   * @param numerator the numerator to truncate
   * @param denominator the denominator to truncate
   * @return conversion rate with truncated numerator and denominator
   */
  static ConversionRate truncate(Unsigned256 numerator, Unsigned256 denominator) {
    Unsigned256 truncatedNumerator = numerator;
    Unsigned256 truncatedDenominator = denominator;
    while (longOverflow(truncatedNumerator) || longOverflow(truncatedDenominator)) {
      truncatedNumerator = truncatedNumerator.divide(Unsigned256.TEN);
      truncatedDenominator = truncatedDenominator.divide(Unsigned256.TEN);
    }
    return new ConversionRate(
        truncatedNumerator.longValueExact(), truncatedDenominator.longValueExact());
  }

  private static boolean longOverflow(Unsigned256 value) {
    return value.compareTo(Unsigned256.create(Long.MAX_VALUE)) > 0;
  }

  record ConversionRate(long numerator, long denominator) {}
}
