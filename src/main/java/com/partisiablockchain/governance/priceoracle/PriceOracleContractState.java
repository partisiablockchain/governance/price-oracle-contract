package com.partisiablockchain.governance.priceoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import java.util.List;

/** State for {@link PriceOracleContract}. */
@Immutable
public final class PriceOracleContractState implements StateSerializable {

  private final BlockchainAddress largeOracleContractAddress;
  private final String referenceContractAddress;
  private final RoundPriceUpdates roundPriceUpdates;
  private final Unsigned256 latestPublishedRoundId;
  private final String symbol;
  private final PriceOracleNodes oracleNodes;
  private final ChallengePeriod challengePeriod;
  private final DisputeInfo disputeInfo;
  private final Decimals decimals;
  private final String chainId;

  @SuppressWarnings("unused")
  private PriceOracleContractState() {
    this.largeOracleContractAddress = null;
    this.roundPriceUpdates = null;
    this.latestPublishedRoundId = null;
    this.symbol = null;
    this.referenceContractAddress = null;
    this.oracleNodes = null;
    this.challengePeriod = null;
    this.disputeInfo = null;
    this.decimals = null;
    this.chainId = null;
  }

  private PriceOracleContractState(
      BlockchainAddress largeOracleContractAddress,
      PriceOracleNodes oracleNodes,
      RoundPriceUpdates roundPriceUpdates,
      Unsigned256 latestPublishedRoundId,
      String symbol,
      ChallengePeriod challengePeriod,
      String referenceContractAddress,
      DisputeInfo disputeInfo,
      Decimals decimals,
      String chainId) {
    this.largeOracleContractAddress = largeOracleContractAddress;
    this.oracleNodes = oracleNodes;
    this.roundPriceUpdates = roundPriceUpdates;
    this.latestPublishedRoundId = latestPublishedRoundId;
    this.symbol = symbol;
    this.challengePeriod = challengePeriod;
    this.referenceContractAddress = referenceContractAddress;
    this.disputeInfo = disputeInfo;
    this.decimals = decimals;
    this.chainId = chainId;
  }

  /**
   * Creates an initial state.
   *
   * @param symbol the coin symbol
   * @param largeOracleContractAddress address of the large oracle contract
   * @param referenceContractAddress address of the reference contract
   * @param decimals information about the different number of decimals used to denote the BYOC
   * @return a new price oracle contract state
   */
  static PriceOracleContractState initial(
      String symbol,
      BlockchainAddress largeOracleContractAddress,
      String referenceContractAddress,
      Decimals decimals,
      String chainId) {
    return new PriceOracleContractState(
        largeOracleContractAddress,
        PriceOracleNodes.create(),
        RoundPriceUpdates.create(),
        Unsigned256.ZERO,
        symbol,
        null,
        referenceContractAddress,
        DisputeInfo.create(),
        decimals,
        chainId);
  }

  static PriceOracleContractState migrateState(StateAccessor oldState) {
    BlockchainAddress largeOracleContractAddress =
        oldState.get("largeOracleContractAddress").blockchainAddressValue();
    PriceOracleNodes oracleNodes =
        PriceOracleNodes.createFromStateAccessor(oldState.get("oracleNodes"));
    RoundPriceUpdates roundPriceUpdates =
        RoundPriceUpdates.createFromStateAccessor(oldState.get("roundPriceUpdates"));
    Unsigned256 latestPublishedRoundId =
        oldState.get("latestPublishedRoundId").cast(Unsigned256.class);
    String symbol = oldState.get("symbol").stringValue();
    ChallengePeriod challengePeriod =
        ChallengePeriod.createFromStateAccessor(oldState.get("challengePeriod"));
    String referenceContractAddress = oldState.get("referenceContractAddress").stringValue();
    DisputeInfo disputeInfo = DisputeInfo.createFromStateAccessor(oldState.get("disputeInfo"));
    Decimals decimals = Decimals.createFromStateAccessor(oldState.get("decimals"));

    String chainId = oldState.get("chainId").stringValue();

    return new PriceOracleContractState(
        largeOracleContractAddress,
        oracleNodes,
        roundPriceUpdates,
        latestPublishedRoundId,
        symbol,
        challengePeriod,
        referenceContractAddress,
        disputeInfo,
        decimals,
        chainId);
  }

  String getSymbol() {
    return symbol;
  }

  BlockchainAddress getLargeOracleContractAddress() {
    return largeOracleContractAddress;
  }

  String getReferenceContractAddress() {
    return referenceContractAddress;
  }

  Unsigned256 getLatestPublishedRoundId() {
    return latestPublishedRoundId;
  }

  boolean hasPriceUpdateFromNode(BlockchainAddress nodeAddress) {
    return roundPriceUpdates.hasPriceUpdateFromNode(nodeAddress);
  }

  boolean isNodePending(BlockchainAddress address) {
    return oracleNodes.isPending(address);
  }

  boolean isRegistered(BlockchainAddress address) {
    return oracleNodes.isRegistered(address);
  }

  List<BlockchainAddress> getRegisteredOracleNodes() {
    return oracleNodes.getRegisteredNodes();
  }

  boolean isNodeOptedOut(BlockchainAddress address) {
    return oracleNodes.isOptedOut(address);
  }

  ChallengePeriod getChallengePeriod() {
    return challengePeriod;
  }

  boolean hasChallengePeriod() {
    return challengePeriod != null;
  }

  boolean hasDispute() {
    return disputeInfo.dispute() != null;
  }

  boolean hasPendingDispute() {
    return disputeInfo.pending();
  }

  Dispute getDispute() {
    return disputeInfo.dispute();
  }

  Dispute getQueuedDispute() {
    return disputeInfo.queued();
  }

  Decimals getDecimals() {
    return decimals;
  }

  PriceOracleContractState registerNode(BlockchainAddress nodeAddress) {
    return setOracleNodes(oracleNodes.registerNode(nodeAddress));
  }

  PriceOracleContractState setNodeToPending(BlockchainAddress nodeAddress) {
    return setOracleNodes(oracleNodes.setNodeToPending(nodeAddress));
  }

  PriceOracleContractState setNodeToOptedOut(BlockchainAddress nodeAddress) {
    return setOracleNodes(oracleNodes.setNodeToOptedOut(nodeAddress));
  }

  PriceUpdates getPriceUpdatesForRound(Unsigned256 roundId) {
    return roundPriceUpdates.getPriceUpdatesForRound(roundId);
  }

  PriceOracleContractState startChallengePeriod(
      long startedAt, Unsigned256 activeChallengePeriodId) {
    return setChallengePeriod(ChallengePeriod.create(activeChallengePeriodId, startedAt));
  }

  PriceOracleContractState resetChallengePeriod() {
    return setChallengePeriod(null);
  }

  boolean isChallengePeriodOver(long blockProductionTime) {
    return hasChallengePeriod() && challengePeriod.hasEnded(blockProductionTime);
  }

  PriceOracleContractState removeOldRounds(Unsigned256 latestRoundId) {
    return setRoundPriceUpdates(roundPriceUpdates.removeOldRounds(latestRoundId));
  }

  PriceOracleContractState removePriceUpdatesForRound(Unsigned256 roundId) {
    return setRoundPriceUpdates(roundPriceUpdates.removePriceUpdatesForRound(roundId));
  }

  PriceOracleContractState removePriceUpdatesForNode(BlockchainAddress maliciousNode) {
    return setRoundPriceUpdates(roundPriceUpdates.removePriceUpdatesForNode(maliciousNode));
  }

  PriceOracleContractState markDisputeAsPending() {
    return setDisputeInfo(disputeInfo.markAsPending());
  }

  PriceOracleContractState beginDispute(Dispute dispute) {
    return setDisputeInfo(disputeInfo.beginDispute(dispute));
  }

  PriceOracleContractState addCounterClaim(RoundData counterClaim) {
    return setDisputeInfo(disputeInfo.addCounterClaim(counterClaim));
  }

  PriceOracleContractState setQueuedDispute(Dispute queuedDispute) {
    return setDisputeInfo(disputeInfo.setQueuedDispute(queuedDispute));
  }

  PriceOracleContractState clearDispute() {
    return setDisputeInfo(disputeInfo.clear());
  }

  PriceOracleContractState removeNode(BlockchainAddress nodeAddress) {
    return setOracleNodes(oracleNodes.remove(nodeAddress));
  }

  PriceOracleContractState addPriceUpdate(RoundData roundData, BlockchainAddress sender) {
    return setRoundPriceUpdates(roundPriceUpdates.addPriceUpdate(roundData, sender));
  }

  PriceOracleContractState setLatestPublishedRoundId(Unsigned256 latestPublishedRoundId) {
    return new PriceOracleContractState(
        largeOracleContractAddress,
        oracleNodes,
        roundPriceUpdates,
        latestPublishedRoundId,
        symbol,
        challengePeriod,
        referenceContractAddress,
        disputeInfo,
        decimals,
        chainId);
  }

  private PriceOracleContractState setOracleNodes(PriceOracleNodes oracleNodes) {
    return new PriceOracleContractState(
        largeOracleContractAddress,
        oracleNodes,
        roundPriceUpdates,
        latestPublishedRoundId,
        symbol,
        challengePeriod,
        referenceContractAddress,
        disputeInfo,
        decimals,
        chainId);
  }

  private PriceOracleContractState setRoundPriceUpdates(RoundPriceUpdates roundPriceUpdates) {
    return new PriceOracleContractState(
        largeOracleContractAddress,
        oracleNodes,
        roundPriceUpdates,
        latestPublishedRoundId,
        symbol,
        challengePeriod,
        referenceContractAddress,
        disputeInfo,
        decimals,
        chainId);
  }

  private PriceOracleContractState setDisputeInfo(DisputeInfo disputeInfo) {
    return new PriceOracleContractState(
        largeOracleContractAddress,
        oracleNodes,
        roundPriceUpdates,
        latestPublishedRoundId,
        symbol,
        challengePeriod,
        referenceContractAddress,
        disputeInfo,
        decimals,
        chainId);
  }

  private PriceOracleContractState setChallengePeriod(ChallengePeriod challengePeriod) {
    return new PriceOracleContractState(
        largeOracleContractAddress,
        oracleNodes,
        roundPriceUpdates,
        latestPublishedRoundId,
        symbol,
        challengePeriod,
        referenceContractAddress,
        disputeInfo,
        decimals,
        chainId);
  }
}
