package com.partisiablockchain.governance.priceoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import java.util.List;
import java.util.stream.Collectors;

/** Holds information about the price oracle nodes including their registration status. */
@Immutable
final class PriceOracleNodes implements StateSerializable {

  private final AvlTree<BlockchainAddress, RegistrationStatus> nodes;

  @SuppressWarnings("unused")
  private PriceOracleNodes() {
    this.nodes = null;
  }

  private PriceOracleNodes(AvlTree<BlockchainAddress, RegistrationStatus> nodes) {
    this.nodes = nodes;
  }

  /**
   * Creates a new price oracle nodes object with a default Avl Tree.
   *
   * @return the newly created object
   */
  static PriceOracleNodes create() {
    return new PriceOracleNodes(AvlTree.create());
  }

  static PriceOracleNodes createFromStateAccessor(StateAccessor stateAccessor) {
    AvlTree<BlockchainAddress, RegistrationStatus> nodes =
        StateAccessorUtil.toAvlTree(
            stateAccessor.get("nodes"), PriceOracleNodes::createStatus, BlockchainAddress.class);
    return new PriceOracleNodes(nodes);
  }

  @SuppressWarnings("EnumOrdinal")
  private static RegistrationStatus createStatus(StateAccessor accessor) {
    Enum<?> enumValue = accessor.cast(Enum.class);
    return RegistrationStatus.values()[enumValue.ordinal()];
  }

  PriceOracleNodes setNodeToPending(BlockchainAddress nodeAddress) {
    return setNodes(nodes.set(nodeAddress, RegistrationStatus.PENDING));
  }

  PriceOracleNodes registerNode(BlockchainAddress nodeAddress) {
    return setNodes(nodes.set(nodeAddress, RegistrationStatus.REGISTERED));
  }

  PriceOracleNodes setNodeToOptedOut(BlockchainAddress nodeAddress) {
    return setNodes(nodes.set(nodeAddress, RegistrationStatus.OPTED_OUT));
  }

  PriceOracleNodes remove(BlockchainAddress nodeAddress) {
    return setNodes(nodes.remove(nodeAddress));
  }

  boolean isPending(BlockchainAddress address) {
    return nodes.getValue(address) == RegistrationStatus.PENDING;
  }

  boolean isRegistered(BlockchainAddress address) {
    return nodes.getValue(address) == RegistrationStatus.REGISTERED;
  }

  boolean isOptedOut(BlockchainAddress address) {
    return nodes.getValue(address) == RegistrationStatus.OPTED_OUT;
  }

  List<BlockchainAddress> getRegisteredNodes() {
    return nodes.keySet().stream().filter(this::isRegistered).collect(Collectors.toList());
  }

  private PriceOracleNodes setNodes(AvlTree<BlockchainAddress, RegistrationStatus> nodes) {
    return new PriceOracleNodes(nodes);
  }
}
